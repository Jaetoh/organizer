/* avoids react warning when running tests */
global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};

global.cancelAnimationFrame = global.clearTimeout;

/* avoids react warning when running tests */
global.requestIdleCallback = (callback) => {
  setTimeout(callback, 0);
};

global.window.matchMedia = () => ({
  addListener: () => null,
  removeListener: () => null,
  matches: false
});
