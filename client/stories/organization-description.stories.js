
import React from 'react';
import { storiesOf } from '@storybook/react';

import ProfileCover from '../src/organization/components/description';

const stories = storiesOf('Organization', module);

stories.add('Description', () => (<ProfileCover
  text="Arsenal Supporter Club of Hong Kong"
/>));
