
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import EventCard from '../src/events/components/event-card';

const stories = storiesOf('Event Card', module);

stories.add('Event card simple', () => (<EventCard
  onClick={action('clicked')}
  imageUrl="http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg"
  title="Trails at Island Lake Conservation Area"
  startDate="1976-04-19T12:59-0500"
  endDate="1999-04-19T12:59-0500"
  location="Island"
  price={1230}
  currency="HKD"
/>));
