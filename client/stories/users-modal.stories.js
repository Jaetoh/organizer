
import React from 'react';

import { storiesOf } from '@storybook/react';

import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducer as modal, show } from 'redux-modal';

import UserListModal from '../src/users/components/users-modal';
import EventCard from '../src/events/components/event-card';


const store = createStore(combineReducers({ modal }));

const stories = storiesOf('Users List Modal', module);

stories.addDecorator(story => <Provider store={store}>{story()}</Provider>);

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}, {
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
},
{
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
},
{
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
},
{
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
},
{
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}];

stories.add('modal', () => (
  <React.Fragment>
    <EventCard
      onClick={() => store.dispatch(show('users-list-modal'))}
      imageUrl="http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg"
      title="Trails at Island Lake Conservation Area"
      startDate="1976-04-19T12:59-0500"
      endDate="1999-04-19T12:59-0500"
      location="Island"
      price={1230}
      currency="HKD"
    />)
    <UserListModal users={users} />
  </React.Fragment>
));
