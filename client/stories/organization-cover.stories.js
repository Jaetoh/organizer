
import React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router';


import ProfileCover from '../src/organization/components/profile-cover';

const stories = storiesOf('Organization', module);

stories.addDecorator(story => (
  <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
));

stories.add('Cover', () => (<ProfileCover
  name="Arsenal Supporter Club of Hong Kong"
  cover_image_url="https://d3vlf99qeg6bpx.cloudfront.net/content/uploads/2016/03/Emirates.jpg"
  profile_image_url="https://risibank.fr/cache/stickers/d120/12031-full.png"
  location="Hong Kong"
  creation_date="2017-04-22T06:00:00Z"
/>));
