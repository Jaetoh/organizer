
import React from 'react';
import { storiesOf } from '@storybook/react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducer as modal } from 'redux-modal';


import EventsList from '../src/events/components/events-list';

const stories = storiesOf('Events List', module);
const store = createStore(combineReducers({ modal }));

stories.addDecorator(story => <Provider store={store}>{story()}</Provider>);

const events = [{
  _id: '1234567890N',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}, {
  _id: '1234567890',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}];

stories.add('Events list simple', () => (<EventsList
  events={events}
  currency="HKD"
/>));
