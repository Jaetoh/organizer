import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';

import UsersList from '../src/users/components/users-list';

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}, {
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}];

const stories = storiesOf('Users list', module);

stories.addDecorator(withKnobs);

stories.add('simple', () => (<UsersList
  users={users}
/>));

