import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs/react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { reducer as modal } from 'redux-modal';

import UsersList from '../src/users/components/add-user-to-event';

const stories = storiesOf('Add user to event', module);
const store = createStore(combineReducers({ modal }));

stories.addDecorator(story => <Provider store={store}>{story()}</Provider>);

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['2234567890']
}, {
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['1234567890']
}];

const events = [{
  _id: '1234567890',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}, {
  _id: '1234567890S',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}];

stories.addDecorator(withKnobs);

stories.add('simple', () => (<UsersList users={users} events={events} />));

