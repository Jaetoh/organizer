
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import noop from 'lodash/noop';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { StaticRouter } from 'react-router';
import { shallow } from 'enzyme';

import EventsList from '../../events-list';

jest.mock('../../../../users/components/users-modal', () => 'users-modal');

let store;

const mockStore = configureStore();

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['2234567890']
}, {
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['1234567890']
}];

const events = [{
  _id: '1234567890',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}, {
  _id: '1234567890S',
  price: 450,
  image_url: 'http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg',
  name: 'I am a super event in Hong kong',
  start_date: '2017-04-22T06:00:00Z',
  end_date: '2017-04-22T06:00:00Z',
  location: '3 Sports Rd, Happy Valley'
}];

describe('events/components/events-list', () => {
  describe('#render', () => {
    beforeEach(() => {
      store = mockStore({});
    });
    it('renders correctly', () => {
      const tree = renderer.create((
        <Provider store={store}>
          <StaticRouter context={{}}>
            <EventsList
              orderEvents={noop}
              users={users}
              showUserListModal={noop}
              events={events}
              currency="HKD"
            />
          </StaticRouter>
        </Provider>));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });

  describe('#handleSortChange', () => {
    it('should update the state with the sort attribut selected', () => {
      const orderEvents = jest.fn();
      const wrapper =
        shallow(<EventsList
          users={users}
          orderEvents={orderEvents}
          showUserListModal={noop}
          events={events}
          currency="HKD"
        />);
      wrapper.find('[id="sort"]').first().simulate('change', { value: 'price' });
      expect(orderEvents).toBeCalledWith({
        descending: false,
        sort: 'price'
      });
      expect(wrapper.state()).toEqual({
        users: [],
        order: { value: false, label: 'Ascending' },
        sort: { value: 'price' }
      });
    });
  });

  describe('#handleOrderChange', () => {
    it('should update the state with the order attribut selected', () => {
      const orderEvents = jest.fn();
      const wrapper =
        shallow(<EventsList
          users={users}
          orderEvents={orderEvents}
          showUserListModal={noop}
          events={events}
          currency="HKD"
        />);
      wrapper.find('[id="order"]').first().simulate('change', { value: true });
      expect(orderEvents).toBeCalledWith({
        descending: true,
        sort: 'name'
      });
      expect(wrapper.state()).toEqual({
        users: [],
        order: { value: true },
        sort: { value: 'name', label: 'Name' }
      });
    });
  });


  describe('#handleClick', () => {
    it('should open the modal when clicking an event card', () => {
      const handleClick = jest.fn();
      const wrapper = shallow((
        <EventsList
          orderEvents={noop}
          users={users}
          showUserListModal={handleClick}
          events={events}
          currency="HKD"
        />
      ));
      wrapper.find('[test="1234567890"]').simulate('click');
      expect(wrapper.state()).toEqual({
        sort: { value: 'name', label: 'Name' },
        order: { value: false, label: 'Ascending' },
        users: [{
          _id: '345678',
          image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
          name: 'Kylian Mbappé',
          email: 'kylian.mbappé@gmail.fr',
          events: ['1234567890']
        }]
      });
      expect(handleClick).toHaveBeenCalled();
    });
  });
});

