import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

import EventCard from '../event-card';
import UserListModal from '../../../users/components/users-modal';
import { Container, WrapperSelect, Wrapper } from './styled-components';

import { EventShape } from '../../types';
import { UserShape } from '../../../users/types';
import Title from '../../../common/title';

const sortOptions = [
  { value: 'name', label: 'Name' },
  { value: 'price', label: 'Price' },
  { value: 'start_date', label: 'Date' },
  { value: 'location', label: 'Location' },
];

const orderOption = [
  { value: false, label: 'Ascending' },
  { value: true, label: 'Descending' },
];

class EventsList extends React.Component {
  static propTypes = {
    events: PropTypes.arrayOf(PropTypes.shape(EventShape)).isRequired,
    users: PropTypes.arrayOf(PropTypes.shape(UserShape)).isRequired,
    currency: PropTypes.string.isRequired,
    showUserListModal: PropTypes.func.isRequired,
    orderEvents: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.renderListItem = this.renderListItem.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleSortChange = this.handleSortChange.bind(this);
    this.handleOrderChange = this.handleOrderChange.bind(this);
  }

  state = {
    users: [],
    sort: sortOptions[0],
    order: orderOption[0],
  };

  handleClick(eventId) {
    return () => {
      this.setState({ users: this.props.users.filter(user => user.events.includes(eventId)) });
      this.props.showUserListModal();
    };
  }

  handleSortChange(selectedOption) {
    this.setState({ sort: selectedOption });
    const { order } = this.state;
    this.props.orderEvents({ sort: selectedOption.value, descending: !!order.value });
  }

  handleOrderChange(selectedOption) {
    this.setState({ order: selectedOption });
    const { sort } = this.state;
    this.props.orderEvents({ sort: sort.value, descending: !!selectedOption.value });
  }

  renderListItem(item) {
    const { _id, price, start_date: startDate,
      end_date: endDate, name, location, image_url: imageUrl } = item;
    return (
      <EventCard
        test={_id} // for test purpose
        key={_id}
        onClick={this.handleClick(_id)}
        imageUrl={imageUrl}
        title={name}
        startDate={startDate}
        endDate={endDate}
        location={location}
        price={price}
        currency={this.props.currency}
      />
    );
  }

  render() {
    const { sort, order } = this.state;
    const { events } = this.props;
    return (
      <React.Fragment>
        <UserListModal users={this.state.users} />
        <Wrapper>
          <Title id="events"> Events </Title>
          <WrapperSelect>
            <Select
              id="sort"
              menuPlacement="top"
              value={sort}
              onChange={this.handleSortChange}
              options={sortOptions}
            />
            <Select
              id="order"
              menuPlacement="top"
              value={order}
              onChange={this.handleOrderChange}
              options={orderOption}
            />
          </WrapperSelect>
        </Wrapper>
        <Container>
          {events.map(this.renderListItem)}
        </Container>
      </React.Fragment>
    );
  }
}

export default EventsList;
