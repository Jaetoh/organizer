import styled from 'styled-components';

import * as l from '../../../styles/layout';

export const Container = styled.section`
  margin-top: 2rem;
  position: relative;

  > * + * {
    margin-top: 1rem;
  }
`;

export const WrapperSelect = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  > * {
    margin-left: 2rem;
    width: 8rem;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;

  @media(max-width: ${l.BP_SMALL}) {
    flex-direction: column;
  }
  justify-content: space-between;
`;
