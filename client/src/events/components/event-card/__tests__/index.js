
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import noop from 'lodash/noop';

import EventCard from '../../event-card';

describe('events/components/event-card', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <EventCard
          onClick={noop}
          imageUrl="http://fibrechannel-europe.com/wp-content/uploads/2018/04/tundra-landscape-1-innovation-ideas-artic-pinterest-biomes.jpg"
          title="Trails at Island Lake Conservation Area"
          startDate="1976-04-19T12:59-0500"
          endDate="1999-04-19T12:59-0500"
          location="Island"
          price={12309}
          currency="HKD"
        />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
