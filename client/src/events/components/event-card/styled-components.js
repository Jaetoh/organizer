import styled from 'styled-components';
import { animated } from 'react-spring';

import * as l from '../../../styles/layout';
import { DATE, PRIMARY } from '../../../styles/colors';

export const Picture = styled.img`
  width: 15rem;

  @media(max-width: ${l.BP_SMALL}) {
    width: 5rem;
  }
`;

export const InfoWrapper = styled.div`
  margin-left: 3rem;
  margin-top: 1rem;
  display: flex;
  flex-direction: column;
  > * + * {
    margin-top: 1rem;
  }

  @media(max-width: ${l.BP_SMALL}) {
    margin-left: 1rem;
    align-items: center;
  }
`;

export const Date = styled.span`
  font-size: 1rem;
  color: ${DATE};
`;

export const Title = styled.span`
  font-size: 1.2rem;
`;

export const Container = styled(animated.article)`
  color: ${PRIMARY};
  height: 10rem;
  overflow:auto;
  background-color: white;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  font-weight: bold;
  transition: box-shadow .2s ease-in-out;
  &:hover {
    box-shadow: 0 10px 32px 0 rgba(0,0,0,0.10);
  }

  @media(max-width: ${l.BP_SMALL}) {
    height: 6rem;
  }
`;
