import React from 'react';
import Moment from 'react-moment';

import PropTypes from 'prop-types';

import { Container, Picture, InfoWrapper, Date, Title } from './styled-components';
import InfoText from '../../../common/info-text';
import Logo from '../../../common/logo';

function formatPrice(price, currency) {
  return `${price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')} ${currency}`;
}

function formatDate(date) {
  return (
    <Moment format="DD, MMM YY, HH">
      {date}
    </Moment>);
}


const EventCard = ({ imageUrl, title,
  startDate, endDate, location, price, currency, onClick }) => (
    <Container onClick={onClick} >
      <Picture src={imageUrl} />
      <InfoWrapper>
        <Date> {formatDate(startDate)} - {formatDate(endDate)} </Date>
        <Title> {title} </Title>
        <InfoText logo={<Logo src="/assets/location.svg" alt="location" />} title={location} />
        <InfoText logo={<Logo src="/assets/coins.svg" alt="coins" />} title={formatPrice(price, currency)} />
      </InfoWrapper>
    </Container>
);

EventCard.propTypes = {
  imageUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  startDate: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default EventCard;
