import PropTypes from 'prop-types';

export const EventShape = {
  _id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  start_date: PropTypes.string.isRequired,
  end_date: PropTypes.string.isRequired,
  image_url: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};
