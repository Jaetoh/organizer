import React from 'react';
import ReactDOM from 'react-dom';
import 'jest-styled-components';

import { Root } from '../main';
import { start } from '../index';

jest.mock('react-dom');

jest.mock('../core', () => ({
  init: jest.fn()
}));

describe('index', () => {
  describe('#start', () => {
    it('should render the app', async () => {
      await start();

      expect(ReactDOM.render).toHaveBeenCalledWith(
        <Root />,
        document.getElementById('root')
      );
    });
  });
});
