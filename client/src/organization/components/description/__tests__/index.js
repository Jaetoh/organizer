
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';


import Description from '../../description';

describe('organization/components/description', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <Description
          text="Arsenal Supporter Club of Hong Kong"
        />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
