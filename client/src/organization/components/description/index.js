import React from 'react';
import PropTypes from 'prop-types';

import Title from '../../../common/title';
import { Container } from './styled-components';


const Description = ({ text }) => (
  <React.Fragment>
    <Title id="about"> About </Title>
    <Container >
      {text}
    </Container>
  </React.Fragment>
);

Description.propTypes = {
  text: PropTypes.string.isRequired,
};

export default Description;
