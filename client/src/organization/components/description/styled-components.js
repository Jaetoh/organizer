import styled from 'styled-components';
import { animated } from 'react-spring';

import { PRIMARY } from '../../../styles/colors';

export const Container = styled(animated.article)`
  padding: 2rem;
  color: ${PRIMARY};
  height: 10rem;
  overflow:auto;
  background-color: white;
  display: flex;
  flex-direction: row;
  font-weight: bold;
  transition: box-shadow .2s ease-in-out;
  &:hover {
    box-shadow: 0 10px 32px 0 rgba(0,0,0,0.10);
  }
`;
