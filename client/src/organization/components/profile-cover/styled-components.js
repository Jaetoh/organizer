import React from 'react';
import styled from 'styled-components';
import { NavHashLink as NavLink } from 'react-router-hash-link';

import * as l from '../../../styles/layout';
import { PRIMARY } from '../../../styles/colors';

export const Container = styled.div`
  height: 10rem;
  background-repeat: no-repeat;
  background-size:cover;
  background-image: url(${props => props.coverImage});
`;

export const Link = styled(props => <NavLink {...props} />)`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 5rem;
  width: 11rem;
  padding-left: 1rem;
  padding-right: 1rem;
  font-size: 1rem;
  text-decoration: none;
  color: ${PRIMARY};
  font-weight: bold;
  cursor: pointer;
  border-left: 1px solid grey;

  @media(max-width: ${l.BP_SMALL}) {
    width: 4rem;
  }
`;

export const CompanyName = styled.div`
  padding-left: 6rem;
  padding-top: 7rem;
  color: white;
  font-weight: bold;
`;

export const Wrapper = styled.div`
  font-family: 'Titillium Web', sans-serif;
  display: flex;
  flex-direction: column;
`;

export const Picture = styled.img`
  border-radius: 50%;
  margin-bottom: 3rem;
  margin-left: 1.5rem;
  margin-right: 1.5rem;
  height: 5rem;
  width: 5rem;
`;

export const WrapperInfos = styled.div`
  width: 8rem;
  font-size: 0.8rem;
  display: flex;
  flex-direction: column;
  > :last-child {
    margin-top: 0.5rem;
  }
`;

export const Menu = styled.div`
  display: flex;
  align-items: center;
  background-color: white;
  height: 5rem;
`;

