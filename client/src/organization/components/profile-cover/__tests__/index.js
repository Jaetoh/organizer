
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';
import { StaticRouter } from 'react-router-dom';


import ProfileCover from '../../profile-cover';

describe('organization/components/profile-cover', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <StaticRouter context={{}}>
          <ProfileCover
            name="Arsenal Supporter Club of Hong Kong"
            cover_image_url="https://d3vlf99qeg6bpx.cloudfront.net/content/uploads/2016/03/Emirates.jpg"
            profile_image_url="https://risibank.fr/cache/stickers/d120/12031-full.png"
            location="Hong Kong"
            creation_date="2017-05-15T06:34:01"
          />
        </StaticRouter>
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
