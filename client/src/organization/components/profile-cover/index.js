import React from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';

import { Container, CompanyName, Wrapper, Menu, Link, WrapperInfos, Picture } from './styled-components';
import InfoText from '../../../common/info-text';
import Logo from '../../../common/logo';


const ProfileCover = ({
  name,
  cover_image_url: cover,
  profile_image_url: profile,
  location,
  creation_date: creationDate
}) => (
  <Wrapper>
    <Container coverImage={cover}>
      <CompanyName> {name} </CompanyName>
    </Container>
    <Menu>
      <Picture alt="test" src={profile} />
      <WrapperInfos>
        <InfoText
          logo={<Logo src="/assets/calendar.svg" alt="calendar" />}
          title={
            <Moment format="DD, MMM YYYY">
              {creationDate}
            </Moment>
          }
        />
        <InfoText logo={<Logo src="/assets/location.svg" alt="location" />} title={location} />
      </WrapperInfos>
      <Link to={`/${name}#about`}> About </Link>
      <Link to={`/${name}#events`}> Events </Link>
      <Link to={`/${name}#guestToEvent`}> Add Guest to Event </Link>
    </Menu>
  </Wrapper>
);

ProfileCover.propTypes = {
  name: PropTypes.string.isRequired,
  cover_image_url: PropTypes.string.isRequired,
  profile_image_url: PropTypes.string.isRequired,
  creation_date: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired
};

export default ProfileCover;
