import React from 'react';
import ReactDOM from 'react-dom';

import './styles/globals';


import { init } from './core';
import { Root } from './main';

export async function start() {
  await init();

  ReactDOM.render(<Root />, document.getElementById('root'));
}

start();
