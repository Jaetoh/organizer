import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { show } from 'redux-modal';

import reducer from './reducer';

import sagas from './sagas';
import * as actions from '../core/actions';

import ProfileCover from '../organization/components/profile-cover';
import Description from '../organization/components/description';
import EventsList from '../events/components/events-list';
import AddUserToEvent from '../users/components/add-user-to-event';
import { EventShape } from '../events/types';
import { UserShape } from '../users/types';

export function mapStateToProps(state) {
  return {
    organization: state.landing.organization,
    events: state.landing.events,
    users: state.landing.users
  };
}

export function getOrganization(organizationName) {
  return {
    type: actions.GET_ORGANIZATION_REQUEST,
    organizationName
  };
}

export function orderEvents(payload) {
  return {
    type: actions.ORDER_EVENTS_REQUEST,
    payload
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    actions: {
      showUserListModal: () => dispatch(show('users-list-modal')),
      getOrganization: bindActionCreators(getOrganization, dispatch),
      orderEvents: bindActionCreators(orderEvents, dispatch)
    }
  };
}


export class Landing extends Component {
  static propTypes = {
    actions: PropTypes.shape({
      getOrganization: PropTypes.func.isRequired,
      showUserListModal: PropTypes.func.isRequired,
      orderEvents: PropTypes.func.isRequired
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.shape({
        organization: PropTypes.string.isRequired
      }).isRequired
    }).isRequired,
    users: PropTypes.arrayOf(PropTypes.shape(UserShape)),
    events: PropTypes.arrayOf(PropTypes.shape(EventShape)),
    organization: PropTypes.shape({
      name: PropTypes.string,
      cover_image_url: PropTypes.string,
      profile_image_url: PropTypes.string,
      created_at: PropTypes.string,
      location: PropTypes.string
    })
  }

  static defaultProps = {
    organization: {},
    events: [],
    users: []
  }

  componentWillMount() {
    const { organization } = this.props.match.params;
    this.props.actions.getOrganization(organization);
  }


  render() {
    const { organization, events, users } = this.props;
    if (!organization.name) {
      return null;
    }
    return (
      <React.Fragment>
        <ProfileCover
          name={organization.name}
          cover_image_url={organization.cover_image_url}
          profile_image_url={organization.profile_image_url}
          location={organization.location}
          creation_date={organization.created_at}
        />
        <Description text={organization.description} />
        <EventsList
          users={users}
          showUserListModal={this.props.actions.showUserListModal}
          orderEvents={this.props.actions.orderEvents}
          events={events}
          currency="HKD"
        />
        <AddUserToEvent users={users} events={events} />
      </React.Fragment>
    );
  }
}

export { reducer, sagas };

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
