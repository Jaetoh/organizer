import { call, all, select, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';


import * as actions from '../core/actions';
import * as service from '../core/services';
import * as selectors from '../core/selectors';

/**
* fetchInfos Responsible to get organization, events and users data
* @param organizationName The name of the organization
*/
export function* fetchInfos({ organizationName }) {
  try {
    const res = yield call(service.getOrganization, organizationName);
    if (!res.data) {
      yield put(push('/'));
    } else {
      const [eventsRes, usersRes] = yield all([
        call(service.getEventsByOrganization, res.data._id, {}),
        call(service.getUsersByOrganization, res.data._id),
      ]);
      yield put({
        type: actions.GET_ORGANIZATION_SUCCESS,
        organization: res.data,
        events: eventsRes.data,
        users: usersRes.data
      });
    }
  } catch (error) {
    yield put(push('/'));
  }
}

/**
* Responsible to get events of an organization
* @param payload Payload containing the sort and order parameters
*/
export function* fetchEvents({ payload }) {
  try {
    const organizationId = yield select(selectors.getOrganizationId);

    // Refetch users
    const eventsRes = yield call(service.getEventsByOrganization, organizationId, payload);

    yield put({
      type: actions.ORDER_EVENTS_SUCCESS,
      events: eventsRes.data
    });
  } catch (err) {
    // Handle error here
  }
}


export default all([
  takeLatest(actions.GET_ORGANIZATION_REQUEST, fetchInfos),
  takeLatest(actions.ORDER_EVENTS_REQUEST, fetchEvents),
]);
