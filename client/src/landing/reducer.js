import * as actions from '../core/actions';

const initialState = {};

export default function landingReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_ORGANIZATION_SUCCESS:
      return {
        ...state,
        organization: action.organization,
        events: action.events,
        users: action.users
      };
    case actions.ADD_USER_TO_EVENT_SUCCESS:
      return {
        ...state,
        users: action.users
      };
    case actions.ORDER_EVENTS_SUCCESS:
      return {
        ...state,
        events: action.events
      };
    default: return state;
  }
}
