import { call, put, all, select } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';
import { push } from 'react-router-redux';


import { fetchInfos, fetchEvents } from '../sagas';
import * as service from '../../core/services';
import * as selectors from '../../core/selectors';

describe('landing/sagas', () => {
  describe('#fetchInfos', () => {
    it('should put a GET_ORGANIZATION_SUCCESS action when fetching successfully data', () => {
      const gen = fetchInfos({ organizationName: 'Trello' });
      let next = gen.next();
      expect(next.value).toEqual(call(service.getOrganization, 'Trello'));
      next = gen.next({ data: { _id: '34567' } });
      expect(next.value).toEqual(all([
        call(service.getEventsByOrganization, '34567', {}),
        call(service.getUsersByOrganization, '34567'),
      ]));
      next = gen.next([{ data: ['event'] }, { data: ['user'] }]);
      expect(next.value).toEqual(put({
        type: 'GET_ORGANIZATION_SUCCESS',
        organization: { _id: '34567' },
        events: ['event'],
        users: ['user']
      }));
      next = gen.next();
      expect(next.done).toBe(true);
    });

    it('should redirect to home page if the organization has not been found', () => {
      const gen = fetchInfos({ organizationName: 'Trello' });
      let next = gen.next();
      expect(next.value).toEqual(call(service.getOrganization, 'Trello'));
      next = gen.next({});
      expect(next.value).toEqual(put(push('/')));
      next = gen.next();
      expect(next.done).toBe(true);
    });

    it('should redirect to home page if something bad happen while retrieving the organization', () => {
      const gen = fetchInfos({ organizationName: 'Trello' });
      let next = gen.next();
      expect(next.value).toEqual(call(service.getOrganization, 'Trello'));
      const error = new SubmissionError('Whoops');
      next = gen.throw(error);
      expect(next.value).toEqual(put(push('/')));
      next = gen.next();
      expect(next.done).toBe(true);
    });
  });

  describe('#fetchEvents', () => {
    it('should put a ORDER_EVENTS_SUCCESS action when fetching successfully data', () => {
      const gen = fetchEvents({ payload: { sort: 'name', order: true } });
      let next = gen.next();
      expect(next.value).toEqual(select(selectors.getOrganizationId));
      next = gen.next('456789');
      expect(next.value).toEqual(call(service.getEventsByOrganization, '456789', { sort: 'name', order: true }));
      next = gen.next({ data: ['event'] });
      expect(next.value).toEqual(put({
        type: 'ORDER_EVENTS_SUCCESS',
        events: ['event']
      }));
      next = gen.next();
      expect(next.done).toBe(true);
    });

    it('should do nothing if something bad happened', () => {
      const gen = fetchEvents({ payload: { sort: 'name', order: true } });
      let next = gen.next();
      expect(next.value).toEqual(select(selectors.getOrganizationId));
      next = gen.next('456789');
      expect(next.value).toEqual(call(service.getEventsByOrganization, '456789', { sort: 'name', order: true }));
      const error = new SubmissionError('Whoops');
      next = gen.throw(error);
      expect(next.done).toBe(true);
    });
  });
});
