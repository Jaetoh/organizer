import 'jest-styled-components';

import reducer from '../../landing/reducer';


describe('landing/reducer', () => {
  describe('landing/GET_ORGANIZATION_SUCCESS', () => {
    it('stores data in the store', () => {
      const action = {
        type: 'GET_ORGANIZATION_SUCCESS',
        organization: {
          _id: '5b4affd89a84d83056545403',
          name: 'HSBC',
          description: 'I am a description',
          created_at: '2018-05-15T04:34:01.000Z',
          location: 'London',
          cover_image_url: 'http://test.com/test.png',
          profile_image_url: 'http://test.com/test.png',
        },
        users: [{
          _id: '345678',
          image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
          name: 'Kylian Mbappé',
          email: 'kylian.mbappé@gmail.fr',
          events: ['2234567890']
        }, {
          _id: '345678',
          image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
          name: 'Kylian Mbappé',
          email: 'kylian.mbappé@gmail.fr',
          events: ['1234567890']
        }],
        events: [{
          _id: '5b4affd89a84d83056545403',
          name: 'Event 1',
          location: 'Paris',
          price: 300,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2017-05-15T04:34:01.000Z',
          end_date: '2017-05-16T04:34:01.000Z',
        }, {
          _id: '5b4affd89a84d83056545400',
          name: 'Z-Event',
          location: 'Hong Kong',
          price: 456,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2018-05-15T04:34:01.000Z',
          end_date: '2018-05-16T04:34:01.000Z',
        }]
      };
      const state = reducer(undefined, action);
      expect(state).toMatchSnapshot();
    });
  });

  describe('landing/ADD_USER_TO_EVENT_SUCCESS', () => {
    it('stores data in the store', () => {
      const action = {
        type: 'ADD_USER_TO_EVENT_SUCCESS',
        users: [{
          _id: '345678',
          image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
          name: 'Kylian Mbappé',
          email: 'kylian.mbappé@gmail.fr',
          events: ['2234567890']
        }, {
          _id: '345678',
          image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
          name: 'Kylian Mbappé',
          email: 'kylian.mbappé@gmail.fr',
          events: ['1234567890']
        }]
      };
      const state = reducer(undefined, action);
      expect(state).toMatchSnapshot();
    });
  });

  describe('landing/ORDER_EVENTS_SUCCESS', () => {
    it('stores data in the store', () => {
      const action = {
        type: 'ORDER_EVENTS_SUCCESS',
        events: [{
          _id: '5b4affd89a84d83056545403',
          name: 'Event 1',
          location: 'Paris',
          price: 300,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2017-05-15T04:34:01.000Z',
          end_date: '2017-05-16T04:34:01.000Z',
        }, {
          _id: '5b4affd89a84d83056545400',
          name: 'Z-Event',
          location: 'Hong Kong',
          price: 456,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2018-05-15T04:34:01.000Z',
          end_date: '2018-05-16T04:34:01.000Z',
        }]
      };
      const state = reducer(undefined, action);
      expect(state).toMatchSnapshot();
    });
  });

  describe('default', () => {
    it('should return the default status', () => {
      const action = { type: 'unknown' };
      const state = reducer(undefined, action);
      expect(state).toMatchSnapshot();
    });
  });
});
