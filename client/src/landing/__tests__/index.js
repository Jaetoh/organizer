import React from 'react';
import 'jest-styled-components';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';

import Landing, { mapStateToProps, mapDispatchToProps,
  Landing as UnconnectedLanding } from '../../landing';

jest.mock('../../users/components/users-modal', () => 'users-modal');


let store;

const mockStore = configureStore();

const events = [{
  _id: '5b4affd89a84d83056545403',
  name: 'Event 1',
  location: 'Paris',
  price: 300,
  image_url: 'http://test.com/test.png',
  organization_id: '5b4affd89a84d83056545403',
  start_date: '2017-05-15T04:34:01.000Z',
  end_date: '2017-05-16T04:34:01.000Z',
}, {
  _id: '5b4affd89a84d83056545400',
  name: 'Z-Event',
  location: 'Hong Kong',
  price: 456,
  image_url: 'http://test.com/test.png',
  organization_id: '5b4affd89a84d83056545403',
  start_date: '2018-05-15T04:34:01.000Z',
  end_date: '2018-05-16T04:34:01.000Z',
}];

const organization = {
  _id: '5b4affd89a84d83056545403',
  name: 'HSBC',
  description: 'I am a description',
  created_at: '2018-05-15T04:34:01.000Z',
  location: 'London',
  cover_image_url: 'http://test.com/test.png',
  profile_image_url: 'http://test.com/test.png',
};

describe('landing', () => {
  beforeEach(() => {
    store = mockStore({
      landing: {
        organization,
        events
      }
    });
  });

  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <Provider store={store}>
          <StaticRouter context={{}}>
            <Landing match={{ params: { organization: '' } }} />
          </StaticRouter>
        </Provider>));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });

  describe('#componentWillMount', () => {
    const actions = {
      getOrganization: jest.fn(),
      showUserListModal: jest.fn(),
      orderEvents: jest.fn()
    };
    it('should emit a getOrganization action', () => {
      mount(<UnconnectedLanding actions={actions} match={{ params: { organization: '' } }} />);

      expect(actions.getOrganization).toBeCalled();
    });
  });

  describe('#mapDispatchToProps', () => {
    it('creates the action props', () => {
      const props = mapDispatchToProps(store.dispatch);
      expect(props).toMatchSnapshot();
    });

    describe('#getOrganization', () => {
      it('dispatches getOrganization action', () => {
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);
        props.actions.getOrganization('Trello');
        expect(dispatch).toHaveBeenCalled();

        expect(dispatch.mock.calls[0][0]).toMatchObject({
          type: 'GET_ORGANIZATION_REQUEST',
          organizationName: 'Trello'
        });
      });
    });

    describe('#orderEvents', () => {
      it('dispatches orderEvents action', () => {
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);
        props.actions.orderEvents({ sort: 'name', order: true });
        expect(dispatch).toHaveBeenCalled();

        expect(dispatch.mock.calls[0][0]).toMatchObject({
          type: 'ORDER_EVENTS_REQUEST',
          payload: { sort: 'name', order: true }
        });
      });
    });

    describe('#showUserListModal', () => {
      it('dispatches showUserListModal action', () => {
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);
        props.actions.showUserListModal();
        expect(dispatch).toHaveBeenCalled();

        expect(dispatch.mock.calls[0][0]).toMatchObject({
          type: '@redux-modal/SHOW',
        });
      });
    });
  });

  describe('#mapStateToProps', () => {
    it('should map state to props', () => {
      const state = {
        landing: {
          organization: {
            _id: '5b4affd89a84d83056545403',
            name: 'HSBC',
            description: 'I am a description',
            created_at: '2018-05-15T04:34:01.000Z',
            location: 'London',
            cover_image_url: 'http://test.com/test.png',
            profile_image_url: 'http://test.com/test.png',
          },
          events: [{
            _id: '5b4affd89a84d83056545403',
            name: 'Event 1',
            location: 'Paris',
            price: 300,
            image_url: 'http://test.com/test.png',
            organization_id: '5b4affd89a84d83056545403',
            start_date: '2017-05-15T04:34:01.000Z',
            end_date: '2017-05-16T04:34:01.000Z',
          }, {
            _id: '5b4affd89a84d83056545400',
            name: 'Z-Event',
            location: 'Hong Kong',
            price: 456,
            image_url: 'http://test.com/test.png',
            organization_id: '5b4affd89a84d83056545403',
            start_date: '2018-05-15T04:34:01.000Z',
            end_date: '2018-05-16T04:34:01.000Z',
          }]
        }
      };
      expect(mapStateToProps(state)).toEqual({
        events: [{
          _id: '5b4affd89a84d83056545403',
          name: 'Event 1',
          location: 'Paris',
          price: 300,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2017-05-15T04:34:01.000Z',
          end_date: '2017-05-16T04:34:01.000Z',
        }, {
          _id: '5b4affd89a84d83056545400',
          name: 'Z-Event',
          location: 'Hong Kong',
          price: 456,
          image_url: 'http://test.com/test.png',
          organization_id: '5b4affd89a84d83056545403',
          start_date: '2018-05-15T04:34:01.000Z',
          end_date: '2018-05-16T04:34:01.000Z',
        }],
        organization: {
          _id: '5b4affd89a84d83056545403',
          name: 'HSBC',
          description: 'I am a description',
          created_at: '2018-05-15T04:34:01.000Z',
          location: 'London',
          cover_image_url: 'http://test.com/test.png',
          profile_image_url: 'http://test.com/test.png',
        },
      });
    });
  });
});
