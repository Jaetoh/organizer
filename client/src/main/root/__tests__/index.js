
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Root from '../../root';

jest.mock('../../app', () => 'App');

describe('main/app', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <Root />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
