import React from 'react';
import { Route } from 'react-router';
import { withRouter, Switch } from 'react-router-dom';

import { Main, Wrapper } from './styled-components';

import Header from '../../common/header';
import Footer from '../../common/footer';

import Landing from '../../landing';


const App = () => (
  <Wrapper>
    <Header logoLink="https://juven.co/" logoPath="https://juven.co/static/common/logo_juven.svg" />
    <Main>
      <Switch>
        <Route exact path="/:organization" component={Landing} />
      </Switch>
    </Main>
    <Footer logoLink="https://juven.co/" logoPath="https://juven.co/static/common/logo_juven.svg" />
  </Wrapper>
);

export default withRouter(App);
