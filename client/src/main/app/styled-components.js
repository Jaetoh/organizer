import styled from 'styled-components';

import { SECONDARY } from '../../styles/colors';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background-color: ${SECONDARY};
  padding-top: 5rem;
`;

export const Main = styled.main`
  flex-grow: 1;
  width: 100%;
  padding: 2.5rem 1rem;
  max-width: 50rem;
  margin: 0 auto;
`;
