import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import { StaticRouter } from 'react-router';

import App from '../../app';

describe('main/app', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <StaticRouter context={{}}>
          <App />
        </StaticRouter>
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
