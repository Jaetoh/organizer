import styled from 'styled-components';

import { PRIMARY } from '../../styles/colors';

const Title = styled.h1`
  font-size: 2.5rem;
  font-weight: 600;
  text-align: left;
  color: ${PRIMARY};
`;

export default Title;
