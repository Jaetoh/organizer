import styled from 'styled-components';
import { PRIMARY } from '../../styles/colors';


export const FooterWrapper = styled.footer`
  justify-self: flex-end;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 5em;
  background-color: #fff;
  padding: 0 1rem;
  color: ${PRIMARY};
`;

export const LogoLink = styled.a`
  justify-self: flex-end;
  display: inline-block;
  margin-bottom: 0.5rem;
  img {
    height: 3rem;
  }
`;
