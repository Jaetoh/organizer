import React from 'react';
import PropTypes from 'prop-types';

import { FooterWrapper, LogoLink } from './styled-components';

const Footer = ({ logoLink, logoPath }) => (
  <FooterWrapper>
    <LogoLink href={logoLink}>
      <img src={logoPath} alt="logo" />
    </LogoLink>
  </FooterWrapper>
);

Footer.propTypes = {
  logoLink: PropTypes.string.isRequired,
  logoPath: PropTypes.string.isRequired,
};

export default Footer;
