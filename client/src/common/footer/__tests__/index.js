import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Footer from '../../footer';

describe('common/footer', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create(<Footer logoLink="test" logoPath="test-path" />);
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
