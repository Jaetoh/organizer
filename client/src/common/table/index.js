import React from 'react';
import { StickyContainer, Sticky } from 'react-sticky';

import StyledTable from './styled-components';

// eslint-disable-next-line react/prop-types
const StickyHead = ({ children, ...props }) => (
  <Sticky {...props}>
    {({ style }) => <div style={style} className="rt-thead sticky">{children}</div>}
  </Sticky>
);

// eslint-disable-next-line react/prop-types
const StickyTable = ({ children, ...props }) => (
  <StickyContainer className="rt-table sticky" role="grid" {...props}>
    { children }
  </StickyContainer>
);


const Table = props => (
  <StyledTable
    {...props}
    defaultPageSize={5}
    resizable
    showPageSizeOptions={false}
    TableComponent={StickyTable}
    TheadComponent={StickyHead}
  />
);

export default Table;
