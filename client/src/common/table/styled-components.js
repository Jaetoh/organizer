import ReactTable from 'react-table';
import styled from 'styled-components';

import { PRIMARY, BG, MUTED } from '../../styles/colors';


/* reset unwanted styles */
const StyledTable = styled(ReactTable)`
  font-family: 'Titillium Web', sans-serif;
  color: ${PRIMARY};
  

  &.ReactTable {
    border: none;
    overflow: auto;
  }

  &.ReactTable .rt-thead .rt-th,
  &.ReactTable .rt-thead .rt-td,
  &.ReactTable .rt-tbody .rt-td {
    border-right: none;
    box-shadow: none;
    padding: 1rem 0.5rem;
    text-align: left;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 0.875em;
    height: 1rem;
  }

  &.ReactTable .rt-thead .rt-th,
  &.ReactTable .rt-thead .rt-td {
    color: ${PRIMARY};
    text-transform: uppercase;
    font-weight: 500;
    font-size: 0.75em;
  }

  &.ReactTable .rt-thead {
    padding: 0 0.5rem;
    background-color: ${BG};
    z-index: 1;
  }

  &.ReactTable .rt-thead[style*="position: fixed"] {
      border-bottom: 1px solid ${MUTED};
    }

  &.ReactTable .rt-thead.-header,
  &.ReactTable .rt-tbody .rt-tr-group {
    box-shadow: none;
    border: none;
  }
  &.ReactTable .rt-tbody .rt-tr {
    padding: 0 0.5rem;
  }

  &.ReactTable .rt-tbody .rt-tr-group:not(:last-child) .rt-tr {
    border-bottom: 1px solid ${MUTED};
  }

  &.ReactTable .rt-tbody .rt-tr-group:not(:last-child) .rt-tr.-padRow {
    border-color: transparent;
  }

  &.ReactTable .rt-tbody {
    background-color: white;
    border: 1px solid ${MUTED};
    border-radius: 2px;
  }

  &.ReactTable .rt-tbody .rt-tr:not(.-padRow):hover {
    background-color: ${BG};
  }

  &.ReactTable .rt-thead .rt-th.-sort-asc,
  &.ReactTable .rt-thead .rt-th.-sort-desc {
    box-shadow: none;
  }

  &.ReactTable .rt-thead .rt-th.-sort-asc > div {
    display: flex;
    align-items: center;
  }
`;

export default StyledTable;
