import React from 'react';
import 'jest-styled-components';
import renderer from 'react-test-renderer';

import Table from '../../table';

const data = [
  { first_name: 'bob', last_name: 'morane' },
  { first_name: 'michel', last_name: 'drucker' }
];

describe('common/table', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const columns = [{
        Header: 'First Name',
        accessor: 'first_name'
      }, {
        Header: 'Last Name',
        accessor: 'last_name'
      }];
      const tree = renderer.create((
        <Table columns={columns} data={data} />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
