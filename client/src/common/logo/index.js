import styled from 'styled-components';

const Logo = styled.img`
  width: 1rem;
  margin-right: 0.5rem;
`;

export default Logo;

