import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connectModal } from 'redux-modal';

import set from 'lodash/set';

import { ModalBackground, ModalContainer, CloseButton, ModalHeader, ModalContent } from './styled-components';

export const wrapElement = WrappedElement =>
  class ModalWrapper extends Component {
    static propTypes = {
      show: PropTypes.bool.isRequired,
      handleHide: PropTypes.func.isRequired
    };

    constructor() {
      super();
      this.refWrapper = this.refWrapper.bind(this);
      this.handleHide = this.handleHide.bind(this);
      this.handleAnimationEnd = this.handleAnimationEnd.bind(this);
      this.handleClick = this.handleClick.bind(this);
    }

    state = {
      willHide: false
    }

    componentDidMount() {
      if (this.wrapper) {
        this.wrapper.addEventListener('click', this.handleClick);
      }
      // remove ability to scroll when modal opens
      set(document.body, 'style.overflow', 'hidden');
    }

    componentDidUpdate() {
      if (this.wrapper) {
        this.wrapper.addEventListener('click', this.handleClick);
      }
    }

    componentWillUnmount() {
      set(document.body, 'style.overflow', null);
      this.wrapper.removeEventListener('animationend', this.handleAnimationEnd);
      this.wrapper.removeEventListener('click', this.handleClick);
    }

    handleHide() {
      this.wrapper.addEventListener('animationend', this.handleAnimationEnd);
      /* fallback in case event is not properly fired (old browser) */
      this.animationend = window.setTimeout(this.handleAnimationEnd, 120);
      this.setState({ willHide: true });
    }

    handleAnimationEnd() {
      window.clearTimeout(this.animationend);
      this.props.handleHide();
    }

    handleClick(e) {
      // the wrapper is the modal background
      if (this.wrapper === e.target) {
        this.props.handleHide();
      }
    }

    refWrapper(el) {
      this.wrapper = el;
    }

    renderVisible() {
      return (
        <ModalBackground innerRef={this.refWrapper} willHide={this.state.willHide}>
          <ModalContainer willHide={this.state.willHide}>
            <ModalHeader><CloseButton onClick={this.handleHide} /></ModalHeader>
            <ModalContent>
              <WrappedElement handleHide={this.handleHide} {...this.props} />
            </ModalContent>
          </ModalContainer>
        </ModalBackground>
      );
    }

    render() {
      return this.props.show ? this.renderVisible() : null;
    }
  };

export default function createModal(options) {
  const connector = connectModal(options);
  return Element => connector(wrapElement(Element));
}
