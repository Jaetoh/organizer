import styled, { keyframes, css } from 'styled-components';
import { easing } from '../../styles/animations';
import close from './icons/close.svg';

const modalBgAppear = keyframes`
  from { opacity: 0; }
  to { opacity: 1; }
`;

const modalContainerAppear = keyframes`
  from { transform: scale(0); }
  to { transform: scale(1); }
`;

const modalDisappear = keyframes`
  from { opacity: 1; }
  to { opacity: 0; }
`;

const modalWillHide = css`
  animation: ${modalDisappear} .1s linear;
`;

export const ModalBackground = styled.div`
  background-color: rgba(0,0,0,.6);
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;
  animation: ${modalBgAppear} .1s linear;
  ${props => (props.willHide ? modalWillHide : null)}
`;

export const ModalContainer = styled.div`
  background-color: #fff;
  border-radius: 0.625rem;
  padding: 1rem;
  animation: ${modalContainerAppear} .2s ${easing};
  max-width: 27.5rem;
  width: 90%;
`;

export const ModalHeader = styled.header`
  display: flex;
  justify-content: flex-end;
`;

export const ModalContent = styled.section`
  padding: 0 1rem 1rem;
  font-size: 0.875em;
`;


export const CloseButton = styled.button.attrs({ type: 'button' })`
  border: none;
  background-color: transparent;
  background: url('${close}') no-repeat center center;
  background-size: 1rem;
  width: 1.5rem;
  height: 1.5rem;
`;
