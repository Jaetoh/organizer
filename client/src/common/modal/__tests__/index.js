import React from 'react';
import renderer from 'react-test-renderer';
import noop from 'lodash/noop';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import 'jest-styled-components';
import * as reduxModal from 'redux-modal';
import configureStore from 'redux-mock-store';

import createModal, { wrapElement } from '../../modal';

jest.useFakeTimers();
const mockStore = configureStore();
let store;

const CustomContent = () => <span id="test">my el</span>;

// Custom mock refs
// return a mock ref object.
function createNodeMock() {
  return {
    addEventListener: () => null
  };
}

describe('common/modal', () => {
  beforeEach(() => {
    store = mockStore({
      modal: {
        test: { show: true }
      }
    });
  });

  describe('#createModal', () => {
    it('returns a component linked with redux-modal', () => {
      const spy = jest.spyOn(reduxModal, 'connectModal');
      const TestModal = createModal({ name: 'test' })(CustomContent);
      expect(spy).toHaveBeenCalledWith({ name: 'test' });
      const wrapper = mount((
        <Provider store={store}>
          <TestModal />
        </Provider>
      ));
      expect(wrapper.find('#test').length).toEqual(1);
    });
  });

  describe('#wrapElement', () => {
    it('renders an element wrapped in a modal container', () => {
      const TestModal = wrapElement(CustomContent);
      const tree = renderer.create(
        <TestModal handleHide={noop} show />,
        { createNodeMock }
      );
      expect(tree).toMatchSnapshot();
    });

    it('renders nothing if the modal is hidden', () => {
      const TestModal = wrapElement(CustomContent);
      const tree = renderer.create(<TestModal handleHide={noop} show={false} />);
      expect(tree).toMatchSnapshot();
    });

    it('cleans the event listener when unmounting', () => {
      const TestModal = wrapElement(CustomContent);
      const wrapper = mount(<TestModal handleHide={noop} show />);
      const spy = jest.spyOn(wrapper.instance().wrapper, 'removeEventListener');
      const handler = wrapper.instance().handleAnimationEnd;
      wrapper.unmount();
      expect(spy).toHaveBeenCalledWith('animationend', handler);
    });
  });

  describe('#componentDidUpdate', () => {
    it('doesnt add the click event listener if the wrapper is not defined', () => {
      const TestModal = wrapElement(CustomContent);
      const wrapper = mount(<TestModal handleHide={noop} show />);
      const spy = jest.spyOn(wrapper.instance().wrapper, 'addEventListener');
      wrapper.setProps({ show: false });
      expect(spy).toHaveBeenCalledTimes(0);
    });
  });

  describe('#handleClose', () => {
    it('closes the modal when called from the container', () => {
      const TestModal = wrapElement(CustomContent);
      const handleHide = jest.fn();
      const wrapper = mount(<TestModal handleHide={handleHide} show />);
      wrapper.find('button').simulate('click');
      jest.runAllTimers();
      expect(handleHide).toHaveBeenCalled();
    });

    it('closes the modal when called from the wrapped element', () => {
      // eslint-disable-next-line react/prop-types
      const MoreCustomContent = ({ handleHide }) => <span><button id="testbutton" onClick={handleHide}>hide</button></span>;
      const TestModal = wrapElement(MoreCustomContent);
      const handleHide = jest.fn();
      const wrapper = mount(<TestModal handleHide={handleHide} show />);
      wrapper.find('#testbutton').simulate('click');
      jest.runAllTimers();
      expect(handleHide).toHaveBeenCalled();
    });
  });

  describe('#handleClick', () => {
    it('closes the modal when clicking outside the modal', () => {
      jest.runAllTimers();

      const TestModal = wrapElement(CustomContent);
      const handleHide = jest.fn();
      const wrapper = mount(<TestModal handleHide={handleHide} show />);

      wrapper.instance().refWrapper('outside-modal');

      wrapper.instance().handleClick({
        target: 'outside-modal',
      });

      expect(handleHide).toHaveBeenCalled();
    });

    it('should not close the modal when clicking inside the modal', () => {
      jest.runAllTimers();

      const TestModal = wrapElement(CustomContent);
      const handleHide = jest.fn();
      const wrapper = mount(<TestModal handleHide={handleHide} show />);

      wrapper.instance().refWrapper('outside-modal');

      wrapper.instance().handleClick({
        target: 'inside-modal',
      });

      expect(handleHide).not.toBeCalled();
    });
  });
});
