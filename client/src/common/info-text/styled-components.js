import styled from 'styled-components';

export const Logo = styled.img`
  width: 2rem;
`;

export const Wrapper = styled.span`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
