import React from 'react';

import PropTypes from 'prop-types';

import { Wrapper } from './styled-components';

const InfoText = ({ logo, title }) => (
  <Wrapper>
    {logo}
    <React.Fragment> {title} </React.Fragment>
  </Wrapper>
);

InfoText.propTypes = {
  logo: PropTypes.element.isRequired,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]).isRequired,
};

export default InfoText;
