
import styled, { css } from 'styled-components';

import * as l from '../../styles/layout';

const stickyHeader = css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 10000;
`;

export const HeaderContainer = styled.header`
  height: 4rem;
  ${stickyHeader}
  display: flex;
  padding: 1rem 2.5rem;
  justify-content: flex-start;
  align-items: center;
  background-color: #fff;
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.1);
  @media(max-width: ${l.BP_SMALL}) {
    padding: 1rem 1.5rem;
  }
`;

export const Logo = styled.img`
  height: 2rem;
`;
