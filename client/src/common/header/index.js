import React from 'react';
import PropTypes from 'prop-types';

import {
  HeaderContainer,
  Logo,
} from './styled-components';

export const Header = ({ logoLink, logoPath, }) => (
  <HeaderContainer>
    <a href={logoLink}>
      <Logo src={logoPath} alt="logo" />
    </a>
  </HeaderContainer>
);

Header.propTypes = {
  logoLink: PropTypes.string.isRequired,
  logoPath: PropTypes.string.isRequired,
};

export default Header;
