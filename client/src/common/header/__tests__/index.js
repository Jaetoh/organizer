import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import Header from '../../footer';

describe('common/header', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create(<Header logoLink="test" logoPath="test-path" />);
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
