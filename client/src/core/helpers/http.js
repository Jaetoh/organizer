import axios from 'axios';

import { version } from '../../../package.json';

const APP_PLATFORM = 'Web';

const request = axios.create({
  headers: {
    app_platform: APP_PLATFORM,
    app_version: version,
    'content-type': 'application/json'
  }
});

export async function setupHttpConfig({ apiEndpoint, defaultTimeout }) {
  request.defaults.baseURL = apiEndpoint;
  request.defaults.timeout = defaultTimeout;
}

export default request;
