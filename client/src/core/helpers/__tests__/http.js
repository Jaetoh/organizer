import request, { setupHttpConfig } from '../http';

const mockConfig = {
  apiEndpoint: 'http://localhost',
  defaultTimeout: 6666,
};

describe('core/helpers/http', () => {
  describe('#setupHttpConfig', () => {
    beforeEach(() => {
      jest.resetModules();
    });

    it('sets the baseURL', async () => {
      await setupHttpConfig(mockConfig);
      expect(request.defaults.baseURL).toEqual('http://localhost');
    });

    it('sets the default timeout', async () => {
      await setupHttpConfig(mockConfig);
      expect(request.defaults.timeout).toEqual(6666);
    });
  });
});
