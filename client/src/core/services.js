import request from './helpers/http';

export function getOrganization(name) {
  return request.get(`/api/organization/${name}`);
}

export function getEventsByOrganization(organizationId, { sort = 'name', descending = false }) {
  return request.get(`/api/events/${organizationId}/?descending=${descending}&sort=${sort}`);
}

export function getUsersByOrganization(organizationId) {
  return request.get(`/api/users/${organizationId}`);
}

export function addUserToEvent({ eventId, userId }) {
  return request
    .patch(`/api/user/${userId}/event`, { event_id: eventId });
}
