import get from 'lodash/get';

/* Timeout values in env are the same that are used on the server, we add 1s
   to them to take into account eventual latency between the front and back */
const ADDITIONAL_TIMEOUT = 1000;

const env = get(window, 'config', {});

const config = {
  /* used for testing purposes, as nock won't do well with relative uris */
  apiEndpoint: '',
  defaultTimeout: (env.defaultTimeout || 10000) + ADDITIONAL_TIMEOUT,
};

export default config;
