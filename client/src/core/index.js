import config from './config';
import { setupHttpConfig } from './helpers/http';

export async function init() {
  await setupHttpConfig(config);
}

export { default as store } from './store';
export { history } from './store';
