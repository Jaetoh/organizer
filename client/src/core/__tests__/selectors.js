import * as selectors from '../selectors';


describe('#getOrganizationId', () => {
  it('should get the organization id', () => {
    const organization = {
      _id: '5b4affd89a84d83056545403',
      name: 'HSBC',
      description: 'I am a description',
      created_at: '2018-05-15T04:34:01.000Z',
      location: 'London',
      cover_image_url: 'http://test.com/test.png',
      profile_image_url: 'http://test.com/test.png',
    };
    const result = selectors.getOrganizationId({
      landing: {
        organization
      }
    });
    expect(result).toEqual('5b4affd89a84d83056545403');
  });
});
