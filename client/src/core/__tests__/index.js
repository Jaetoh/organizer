import { setupHttpConfig } from '../helpers/http';

import { init } from '../../core';

jest.mock('../helpers/http', () => ({ setupHttpConfig: jest.fn() }));

describe('core/config', () => {
  describe('#init', () => {
    it('inits everything', async () => {
      await init();
      expect(setupHttpConfig).toHaveBeenCalled();
    });
  });
});
