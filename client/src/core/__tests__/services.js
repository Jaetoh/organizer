import nock from 'nock';
import * as service from '../services';
import request, { setupHttpConfig } from '../helpers/http';

request.defaults.headers['user-agent'] = 'test-ua';

describe('core/services', () => {
  let postBody;

  beforeAll(async () => {
    await setupHttpConfig({ apiEndpoint: 'http://localhost' });
  });

  afterEach(() => {
    nock.cleanAll();
  });

  describe('#getOrganization', () => {
    it('should get an organization', async () => {
      const apiNock = nock('http://localhost')
        .get('/api/organization/Trello', (actualBody) => {
          postBody = actualBody;
          return true;
        })
        .reply(200);

      await service.getOrganization('Trello');

      expect(postBody).toEqual('');

      expect(apiNock.isDone()).toBe(true);
    });
  });

  describe('#getEventsByOrganization', () => {
    it('should get events by organization', async () => {
      const apiNock = nock('http://localhost')
        .get('/api/events/456789/?descending=false&sort=name', (actualBody) => {
          postBody = actualBody;
          return true;
        })
        .reply(200);

      await service.getEventsByOrganization('456789', {});

      expect(postBody).toEqual('');

      expect(apiNock.isDone()).toBe(true);
    });
  });

  describe('#getUsersByOrganization', () => {
    it('should get users by organization', async () => {
      const apiNock = nock('http://localhost')
        .get('/api/users/456789', (actualBody) => {
          postBody = actualBody;
          return true;
        })
        .reply(200);

      await service.getUsersByOrganization('456789');

      expect(postBody).toEqual('');

      expect(apiNock.isDone()).toBe(true);
    });
  });

  describe('#addUserToEvent', () => {
    it('should make a user guest of an event', async () => {
      const apiNock = nock('http://localhost')
        .patch('/api/user/2345678/event', (actualBody) => {
          postBody = actualBody;
          return true;
        })
        .reply(200);

      await service.addUserToEvent({ eventId: '123456', userId: '2345678' });

      expect(postBody).toEqual({ event_id: '123456' });

      expect(apiNock.isDone()).toBe(true);
    });
  });
});
