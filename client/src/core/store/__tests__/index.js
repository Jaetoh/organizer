import { configureStore } from '../../store';

describe('core/store/', () => {
  describe('#configureStore', () => {
    it('should return a new store', () => {
      const store = configureStore();

      expect(store).toBeDefined();
      expect(store).toHaveProperty('dispatch');
      expect(store).toHaveProperty('getState');
    });
  });
});
