import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import { sagaMiddleware, mainSaga } from './sagas';
import rootReducer from './rootReducer';


export const history = createHistory();

let middleware = applyMiddleware(routerMiddleware(history), sagaMiddleware);

/* istanbul ignore next line */
if (process.env.NODE_ENV === 'development') {
  middleware = composeWithDevTools(middleware);
}

export function configureStore() {
  return createStore(rootReducer, middleware);
}

export default configureStore();

sagaMiddleware.run(mainSaga);
