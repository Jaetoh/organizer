import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import { routerReducer as router } from 'react-router-redux';
import { reducer as modal } from 'redux-modal';

import { reducer as landing } from '../../landing';

export default combineReducers({
  form,
  router,
  modal,
  landing,
});
