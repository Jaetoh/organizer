import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import { sagas as landingSagas } from '../../landing';
import userSaga from '../../users/sagas';

export const sagaMiddleware = createSagaMiddleware();

export function* mainSaga() {
  yield all([
    landingSagas,
    userSaga
  ]);
}
