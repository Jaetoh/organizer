import get from 'lodash/get';

export function getOrganizationId(state) {
  return get(state, 'landing.organization._id');
}
