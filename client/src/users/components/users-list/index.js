import React from 'react';
import PropTypes from 'prop-types';

import Table from '../../../common/table';
import { UserShape } from '../../types';
import { Picture } from './styled-components';


const columns = [
  {
    Header: 'Photo',
    accessor: 'image_url',
    width: 100,
  }, {
    Header: 'Name',
    accessor: 'name',
  }, {
    Header: 'email',
    accessor: 'email',
  }];

const UsersList = ({ users }) => (
  <Table
    data={users.map(user => ({ ...user, image_url: <Picture src={user.image_url} /> }))}
    columns={columns}
  />
);

UsersList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape(UserShape)).isRequired,
};

export default UsersList;
