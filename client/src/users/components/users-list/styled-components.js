import styled from 'styled-components';

export const Picture = styled.img`
  border-radius: 50%;
  width: 1.5rem;
  height: 1.5rem;
`;
