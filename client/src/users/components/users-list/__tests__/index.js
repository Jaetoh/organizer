
import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import UsersList from '../../users-list';

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}, {
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}];

describe('users/components/users-list', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <UsersList
          users={users}
        />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
