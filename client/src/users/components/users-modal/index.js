import React from 'react';
import PropTypes from 'prop-types';

import createModal from '../../../common/modal';
import { UserShape } from '../../types';
import UsersList from '../users-list';

import { Title } from './styled-components';

export const UsersModal = ({ users }) => (
  <React.Fragment>
    <Title> Guests List </Title>
    <UsersList users={users} />
  </React.Fragment>
);

UsersModal.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape(UserShape)).isRequired,
};

const connector = createModal({ name: 'users-list-modal', destroyOnHide: true });
export default connector(UsersModal);
