import styled from 'styled-components';

import { PRIMARY } from '../../../styles/colors';


export const Title = styled.h1`
  font-family: 'Titillium Web', sans-serif;
  font-size: 2rem;
  font-weight: 600;
  text-align: left;
  color: ${PRIMARY};
  margin-bottom: 1rem;
`;
