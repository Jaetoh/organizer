import React from 'react';
import renderer from 'react-test-renderer';
import 'jest-styled-components';

import { UsersModal } from '../../users-modal';

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}, {
  _id: '34567A8',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr'
}];

describe('users/components/users-list', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <UsersModal users={users} />
      ));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });
});
