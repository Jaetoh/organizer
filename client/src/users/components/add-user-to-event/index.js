import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'react-materialize';
import { bindActionCreators } from 'redux';

import { EventShape } from '../../../events/types';
import { UserShape } from '../../types';
import * as actions from '../../../core/actions';
import Title from '../../../common/title';
import { Wrapper } from './styled-components';

export function addUserToEvent(payload) {
  return {
    type: actions.ADD_USER_TO_EVENT_REQUEST,
    payload
  };
}

export function mapDispatchToProps(dispatch) {
  return {
    actions: {
      addUserToEvent: bindActionCreators(addUserToEvent, dispatch)
    }
  };
}

export class AddUserToEvent extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({
      addUserToEvent: PropTypes.func.isRequired,
    }).isRequired,
    users: PropTypes.arrayOf(PropTypes.shape(UserShape)),
    events: PropTypes.arrayOf(PropTypes.shape(EventShape)),
  }

  static defaultProps = {
    events: [],
    users: []
  }

  constructor() {
    super();
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleEventChange = this.handleEventChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  state = {
    eventSelected: {},
    userSelected: {},
  }

  handleEventChange(selectedOption) {
    this.setState({ eventSelected: selectedOption });
  }

  handleUserChange(selectedOption) {
    this.setState({ userSelected: selectedOption });
  }

  handleClick() {
    const { eventSelected, userSelected } = this.state;
    this.props.actions.addUserToEvent({ eventId: eventSelected.value, userId: userSelected.value });
    this.setState({ eventSelected: {}, userSelected: {} });
  }

  render() {
    const { eventSelected, userSelected } = this.state;
    const { users, events } = this.props;

    return (
      <React.Fragment>
        <Title id="guestToEvent"> Add Guest to Events </Title>
        <Wrapper>
          <Select
            id="event"
            menuPlacement="top"
            value={eventSelected}
            onChange={this.handleEventChange}
            options={events.map(event => ({ value: event._id, label: event.name }))}
          />
          <Select
            id="user"
            menuPlacement="top"
            value={userSelected}
            onChange={this.handleUserChange}
            options={users
              .filter(user => !user.events.includes(eventSelected.value))
              .map(user => ({ value: user._id, label: user.name }))}
          />
          <Button
            disabled={!(eventSelected.value && userSelected.value)}
            onClick={this.handleClick}
            waves="light"
            id="button-test"
          >
            Add Guest To Events
          </Button>
        </Wrapper>
      </React.Fragment>
    );
  }
}

export default connect(null, mapDispatchToProps)(AddUserToEvent);
