import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-top: 2rem;

  > * {
    margin-left: 2rem;
    width: 8rem;
  }
`;
