import React from 'react';
import 'jest-styled-components';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';
import { shallow } from 'enzyme';

import AddUserToEvent, { AddUserToEvent as UnconnectedAddUserToEvent, mapDispatchToProps } from '../../add-user-to-event';


let store;

const mockStore = configureStore();

const events = [{
  _id: '5b4affd89a84d83056545403',
  name: 'Event 1',
  location: 'Paris',
  price: 300,
  image_url: 'http://test.com/test.png',
  organization_id: '5b4affd89a84d83056545403',
  start_date: '2017-05-15T04:34:01.000Z',
  end_date: '2017-05-16T04:34:01.000Z',
}, {
  _id: '5b4affd89a84d83056545400',
  name: 'Z-Event',
  location: 'Hong Kong',
  price: 456,
  image_url: 'http://test.com/test.png',
  organization_id: '5b4affd89a84d83056545403',
  start_date: '2018-05-15T04:34:01.000Z',
  end_date: '2018-05-16T04:34:01.000Z',
}];

const users = [{
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['2234567890']
}, {
  _id: '345678',
  image_url: 'https://cdn.images.dailystar.co.uk/dynamic/204/photos/882000/620x/Kylian-Mbappe-716616.jpg',
  name: 'Kylian Mbappé',
  email: 'kylian.mbappé@gmail.fr',
  events: ['1234567890']
}];

const organization = {
  _id: '5b4affd89a84d83056545403',
  name: 'HSBC',
  description: 'I am a description',
  created_at: '2018-05-15T04:34:01.000Z',
  location: 'London',
  cover_image_url: 'http://test.com/test.png',
  profile_image_url: 'http://test.com/test.png',
};

describe('AddUserToEvent', () => {
  beforeEach(() => {
    store = mockStore({
      landing: {
        organization,
        events,
        users
      }
    });
  });

  describe('#render', () => {
    it('renders correctly', () => {
      const tree = renderer.create((
        <Provider store={store}>
          <StaticRouter context={{}}>
            <AddUserToEvent users={users} events={events} />
          </StaticRouter>
        </Provider>));
      expect(tree.toJSON()).toMatchSnapshot();
    });
  });

  describe('#handleEventChange', () => {
    it('should update the state with the event selected', () => {
      const actions = {
        addUserToEvent: jest.fn()
      };
      const wrapper =
        shallow(<UnconnectedAddUserToEvent actions={actions} users={users} events={events} />);
      wrapper.find('[id="event"]').first().simulate('change', { value: '456' });
      expect(wrapper.state()).toEqual({
        eventSelected: { value: '456' },
        userSelected: {}
      });
    });
  });

  describe('#handleUserChange', () => {
    it('should update the state with the user selected', () => {
      const actions = {
        addUserToEvent: jest.fn()
      };
      const wrapper =
        shallow(<UnconnectedAddUserToEvent actions={actions} users={users} events={events} />);
      wrapper.find('[id="user"]').first().simulate('change', { value: '456' });
      expect(wrapper.state()).toEqual({
        eventSelected: {},
        userSelected: { value: '456' }
      });
    });
  });

  describe('#handleClick', () => {
    it('should update the state with the user selected', () => {
      const actions = {
        addUserToEvent: jest.fn()
      };
      const wrapper =
        shallow(<UnconnectedAddUserToEvent actions={actions} users={users} events={events} />);
      wrapper.setState({ eventSelected: { value: '456' }, userSelected: { value: '456' } });
      wrapper.find('[id="button-test"]').at(0).simulate('click', {});
      expect(actions.addUserToEvent).toBeCalledWith({
        eventId: '456',
        userId: '456'
      });
    });
  });

  describe('#mapDispatchToProps', () => {
    it('creates the action props', () => {
      const props = mapDispatchToProps(store.dispatch);
      expect(props).toMatchSnapshot();
    });

    describe('#addUserToEvent', () => {
      it('dispatches addUserToEvent action', () => {
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);
        props.actions.addUserToEvent({ eventId: '12', userId: '45678' });
        expect(dispatch).toHaveBeenCalled();

        expect(dispatch.mock.calls[0][0]).toMatchObject({
          type: 'ADD_USER_TO_EVENT_REQUEST',
          payload: { eventId: '12', userId: '45678' }
        });
      });
    });
  });
});
