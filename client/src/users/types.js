import PropTypes from 'prop-types';

export const UserShape = {
  _id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  image_url: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired
};
