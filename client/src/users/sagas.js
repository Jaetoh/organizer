import { call, all, put, takeLatest, select } from 'redux-saga/effects';

import * as actions from '../core/actions';
import * as service from '../core/services';
import * as selectors from '../core/selectors';

/**
* Responsible to add a user to an event
* @param payload Payload containing the event id and the user id
*/
export function* addUserToEvent({ payload }) {
  try {
    const organizationId = yield select(selectors.getOrganizationId);

    yield call(service.addUserToEvent, payload);

    // Refetch users
    const usersRes = yield call(service.getUsersByOrganization, organizationId);

    yield put({
      type: actions.ADD_USER_TO_EVENT_SUCCESS,
      users: usersRes.data
    });
  } catch (err) {
    // Handle error here
  }
}


export default all([
  takeLatest(actions.ADD_USER_TO_EVENT_REQUEST, addUserToEvent),
]);
