import { call, put, select } from 'redux-saga/effects';
import { SubmissionError } from 'redux-form';


import { addUserToEvent } from '../sagas';
import * as service from '../../core/services';
import * as selectors from '../../core/selectors';

describe('users/sagas', () => {
  describe('#addUserToEvent', () => {
    it('should put a ADD_USER_TO_EVENT_SUCCESS action when adding user to an event', () => {
      const gen = addUserToEvent({ payload: { eventId: '456', userId: '567' } });
      let next = gen.next();
      expect(next.value).toEqual(select(selectors.getOrganizationId));
      next = gen.next('456789');
      expect(next.value).toEqual(call(service.addUserToEvent, { eventId: '456', userId: '567' }));
      next = gen.next({ data: { _id: '34567' } });
      expect(next.value).toEqual(call(service.getUsersByOrganization, '456789'));
      next = gen.next({ data: ['user'] });
      expect(next.value).toEqual(put({
        type: 'ADD_USER_TO_EVENT_SUCCESS',
        users: ['user']
      }));
      next = gen.next();
      expect(next.done).toBe(true);
    });

    it('should not update the store if the service fails', () => {
      const gen = addUserToEvent({ payload: { eventId: '456', userId: '567' } });
      let next = gen.next();
      expect(next.value).toEqual(select(selectors.getOrganizationId));
      next = gen.next('456789');
      expect(next.value).toEqual(call(service.addUserToEvent, { eventId: '456', userId: '567' }));
      const error = new SubmissionError('Whoops');
      next = gen.throw(error);
      expect(next.done).toBe(true);
    });
  });
});
