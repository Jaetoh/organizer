FROM node:8.9

ARG NPM_TOKEN
ARG BUILD_ENV=production

WORKDIR /app

COPY lerna.json package.json yarn.lock .yarnrc .npmrc ./
COPY ./client/package.json ./client/
COPY ./server/package.json ./server/

RUN npm install --global lerna

COPY . /app

RUN lerna bootstrap -- --production --no-optional

RUN lerna run postinstall

CMD ["lerna", "run", "start"]
