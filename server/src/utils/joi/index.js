'use strict';

const Joi = require('joi');
const dateExtend = require('joi-date-extensions');

const objectIdExtend = require('./ObjectId.joi');

const joiExtends = [
  objectIdExtend,
  dateExtend,
];

const extendedJoi = Joi.extend(joiExtends);

module.exports = {
  Joi: extendedJoi,
};
