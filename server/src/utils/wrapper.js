
'use strict';

/**
 * Convert an async controller function to an express compatible version.
 *
 * @param {Function} fn - the controller function
 * @returns {Function} async express compatible version
 */
function wrapper(fn) {
  if (!fn) {
    throw new Error('The function passed to wrap must not be null or undefined');
  }

  return async function wrapped(...args) {
    const next = args[args.length - 1];

    try {
      await fn(...args);
    } catch (err) {
      next(err);
    }
  };
}

module.exports = wrapper;
