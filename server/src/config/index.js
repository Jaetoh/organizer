'use strict';

const path = require('path');

const root = path.resolve(__dirname, '../..');

const mongodbConfig = {
  databases: [{
    name: 'organizer',
    url: process.env.MONGO_URL || 'mongodb://localhost:27017/organizer',
    options: {
      ssl: process.env.MONGO_USE_SSL === 'true',
      sslValidate: process.env.MONGO_SSL_VALIDATE === 'true',
      sslCA: [Buffer.from(process.env.MONGO_SSL_CERT || '', 'utf-8')],
    },
  },
  ],
};

module.exports = {
  web: {
    port: parseInt(process.env.PORT, 10) || 3001,
    serveHtml: process.env.NODE_ENV !== 'development',
    clientRoot: path.resolve(root, process.env.CLIENT_PATH || '../client/build'),
  },
  services: {
    mongodb: mongodbConfig,
  },
};
