'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const expressMiddleware = require('express-middleware');

const api = require('../api');
const config = require('../config');
const errorHandler = require('../middlewares/errorHandler');

/**
 * Setup Express Config
 * @param  {Object} app Express instance
 * @param {Object} services Object containing services like mongo, logger, etc..
 * @return {void}
 */
module.exports = function setup(app, services) {
  /** middlewares */
  app.use(expressMiddleware.requestId());
  app.use(expressMiddleware.childLogger(services.logger));

  /** Body parser */
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  /** Api-doc */
  app.use('/apidoc', express.static('apidoc'));

  api(app, services);

  /* istanbul ignore next line */
  if (config.web.serveHtml) {
    services.logger.info({ path: config.web.clientRoot }, '[config#express#configure] Serving static HTML');
    app.use(express.static(config.web.clientRoot, { index: false }));
    app.get('*', (req, res) => res.sendFile(path.resolve(config.web.clientRoot, 'index.html')));
  }

  app.use(errorHandler(services));

  /**  App configuration. */
  app.set('port', config.web.port);

  return app;
};
