'use strict';

/**
 * Build Bad Request error from JOI ValidationError exception
 * @param  {Error} err Error object to format
 * @param  {Object} body Body of the response
 * @return {Boolean} true if error has been built, false otherwise
 */
function assignJoiError(err, body) {
  if (err.name === 'ValidationError') {
    err.status = 400;
    err.details.forEach((detail) => {
      body[detail.context.key] = [detail.message];
    });
    return true;
  }
  return false;
}

/**
 * Error handler middleware
 * @param  {Object} services Services object
 * @return {void}
 */
function middleware(services) {
  return (err, req, res, next) => {
    if (!err) {
      return next();
    }

    const body = {};

    if (!assignJoiError(err, body)) {
      body.detail = err.message;
    }

    const status = err.status || 500;

    body.status = status;

    if (status >= 500) {
      services.logger.error({ err });
    } else {
      services.logger.info(err);
    }
    return res.status(status).send(body);
  };
}

module.exports = middleware;

