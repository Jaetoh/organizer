'use strict';

const { Joi } = require('../utils/joi');

const organizationSchema = Joi.object().keys({
  _id: Joi.objectId(),
  name: Joi.string().required(),
  description: Joi.string().required(),
  created_at: Joi.date().required(),
  location: Joi.string().required(),
  profile_image_url: Joi.string().required(),
  cover_image_url: Joi.string().required(),
});

const COLLECTION = 'organizations';
const DATABASE = 'organizer';

/**
 * Create collection indexes
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {void}
 */
async function createIndexes(mongodb) {
  await collection(mongodb).createIndex(
    { name: 1 },
    { unique: true, background: true }
  );
}

/**
 * Validate schema consistency
 *
 * @param {Object} organization The organization document to validate
 * @return {Object} The organization document validated
 */
function validateSchema(organization) {
  return Joi.attempt(organization, organizationSchema);
}

/**
 * Create a organization document
 *
 * @param {Object} payload The payload to create the organization document
 * @param {Object} mongodb A pointer to the connection object.
 * @return {Object} The document inserted
 */
async function create(payload, mongodb) {
  const payloadValidated = validateSchema(payload);

  const result = await collection(mongodb)
    .insert(payloadValidated);

  return result.ops[0];
}

/**
 * Returns a pointer to the "organizations" collection.
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {Object} The pointer to the "organizations" collection
 */
function collection(mongodb) {
  return mongodb.db(DATABASE).collection(COLLECTION);
}

/**
 * Find one organization
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {String} organizationName The organization name
 * @return {Promise.<Object>} Booking object
 */
function findOne(mongodb, organizationName) {
  return collection(mongodb).findOne({ name: organizationName });
}

module.exports = {
  collection,
  createIndexes,
  findOne,
  create,
};
