'use strict';

const { Joi } = require('../utils/joi');

const userSchema = Joi.object().keys({
  _id: Joi.objectId(),
  name: Joi.string().required(),
  email: Joi.string().required(),
  image_url: Joi.string().required(),
  events: Joi.array().items(Joi.objectId()).single(),
  organizations: Joi.array().items(Joi.objectId()).single(),
});

const COLLECTION = 'users';
const DATABASE = 'organizer';

/**
 * Create collection indexes
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {void}
 */
async function createIndexes(mongodb) {
  await collection(mongodb).createIndex(
    { organization_id: 1 },
    { background: true }
  );
}

/**
 * Validate schema consistency
 *
 * @param {Object} user The user document to validate
 * @return {Object} The user document validated
 */
function validateSchema(user) {
  return Joi.attempt(user, userSchema);
}

/**
 * Create a user document
 *
 * @param {Object} payload The payload to create the user document
 * @param {Object} mongodb A pointer to the connection object.
 * @return {Object} The document inserted
 */
async function create(payload, mongodb) {
  const payloadValidated = validateSchema(payload);

  const result = await collection(mongodb)
    .insert(payloadValidated);

  return result.ops[0];
}

/**
 * Returns a pointer to the "users" collection.
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {Object} The pointer to the "users" collection
 */
function collection(mongodb) {
  return mongodb.db(DATABASE).collection(COLLECTION);
}

/**
 * Find users by organization
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {String} organizationId The organization to look up
 * @param {Integer} page The page to query
 * @param {Integer} limit Limit the documents to retrieve
 * @return {Promise} The document
 */
function findByOrganization(mongodb, organizationId) {
  return collection(mongodb)
    .find({ organizations: organizationId })
    .sort({ _id: -1 })
    .toArray();
}

/**
 * Find user
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {Integer} userId The user id
 * @return {Promise.<Object>} Booking object
 */
function findOne(mongodb, userId) {
  return collection(mongodb).findOne({ _id: userId });
}

/**
 * Add an event to a user
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {String} eventId The event id
 * @param {Integer} userId The user id
 * @param {Integer} limit Limit the documents to retrieve
 * @return {Promise} The document updated
 */
function addEvent(mongodb, eventId, userId) {
  return collection(mongodb).findOneAndUpdate(
    {
      _id: userId,
    },
    {
      $push: {
        events: eventId,
      },
    }
  );
}

module.exports = {
  collection,
  findByOrganization,
  addEvent,
  findOne,
  create,
  createIndexes,
};
