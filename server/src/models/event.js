'use strict';

const { Joi } = require('../utils/joi');

const eventSchema = Joi.object().keys({
  _id: Joi.objectId(),
  name: Joi.string().required(),
  location: Joi.string().required(),
  start_date: Joi.date().required(),
  image_url: Joi.string().required(),
  price: Joi.number().required(),
  end_date: Joi.date().required(),
  organization_id: Joi.objectId(),
});

const COLLECTION = 'events';
const DATABASE = 'organizer';

/**
 * Create collection indexes
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {void}
 */
async function createIndexes(mongodb) {
  await collection(mongodb).createIndex(
    { organization_id: 1 },
    { background: true }
  );
}

/**
 * Validate schema consistency
 *
 * @param {Object} event The event document to validate
 * @return {Object} The event document validated
 */
function validateSchema(event) {
  return Joi.attempt(event, eventSchema);
}

/**
 * Create an event document
 *
 * @param {Object} payload The payload to create the event document
 * @param {Object} mongodb A pointer to the connection object.
 * @return {Object} The document inserted
 */
async function create(payload, mongodb) {
  const payloadValidated = validateSchema(payload);

  const result = await collection(mongodb)
    .insert(payloadValidated);

  return result.ops[0];
}

/**
 * Returns a pointer to the "events" collection.
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @returns {Object} The pointer to the "events" collection
 */
function collection(mongodb) {
  return mongodb.db(DATABASE).collection(COLLECTION);
}
/**
 * Find event
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {String} eventId The event id
 * @return {Promise.<Object>} Booking object
 */
function findOne(mongodb, eventId) {
  return collection(mongodb).findOne({ _id: eventId });
}

/**
 * Find events by organization
 *
 * @param {Object} mongodb A pointer to the connection object.
 * @param {String} organizationId The organization to look up
 * @param {Integer} page The page to query
 * @param {Integer} limit Limit the documents to retrieve
 * @return {Promise} The document
 */
function findByOrganization(mongodb, {
  organizationId, page, limit, sort, descending,
}) {
  return collection(mongodb)
    .find({ organization_id: organizationId })
    .skip(limit * (page - 1)).limit(limit)
    .sort({ [sort]: descending ? -1 : 1 })
    .hint({ organization_id: 1 })
    .toArray();
}

module.exports = {
  collection,
  createIndexes,
  create,
  findOne,
  findByOrganization,
  validateSchema,
};
