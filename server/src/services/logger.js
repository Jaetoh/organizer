'use strict';

const bunyan = require('bunyan');

const log = bunyan.createLogger({ name: 'organizer' });

module.exports = log;

