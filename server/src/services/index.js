'use strict';

const MongodbConnector = require('./mongo');

const logger = require('./logger');

/**
 * Creates the service provider.
 *
 * @param {Object} config Configuration of the services
 * @returns {Object} service provider
 */
function createServiceProvider(config) {
  return {
    logger,
    mongodb: new MongodbConnector(config.services.mongodb, logger),
  };
}

module.exports = createServiceProvider;
