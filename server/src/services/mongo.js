'use strict';

/* eslint-disable */

const { MongoClient } = require('mongodb');

module.exports = class MongoDbConnector {
  /**
   * MongoDB connector class
   *
   * @param {object} config Connector configuration
   * @param {object} logger logger service
   */
  constructor(config, logger) {
    /**
     * Connector configuration
     */
    this.config = config;

    /**
     * Map of connected databases
     */
    this.DATABASES = new Map();

    this.logger = logger;
  }

  /**
   * Connect the databases
   *
   * @throws Error on connection issue
   *
   * @returns {Promise<Map>} The map of databases connections
   */
  async connect() {
    const databases = this.config.databases;
    for (const database of databases) {
      const db = await MongoClient.connect(database.url, database.options);
      this.DATABASES.set(database.name, db);

      this.logger.info({ db: database.name }, '> Database connected');
    }

    return this.DATABASES;
  }

  /**
   * Disconnect the database
   *
   */
  async disconnect() {
    for (const [name, db] of this.DATABASES) {
      try {
        await db.close();

        this.logger.info({ db: name }, '> Mongodb disconnected');
      } finally {
        this.DATABASES.delete(name);
      }
    }

    return this.DATABASES;
  }

  /**
   * Returns a database connection
   *
   * @param {string} databaseName The database name
   * @returns {Db}
   */
  db(databaseName) {
    return this.DATABASES.get(databaseName);
  }
};
