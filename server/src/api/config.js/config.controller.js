'use strict';

/**
 * Get the client config as a JS file
 *
 * @param {Object} services : Object containing the logger service
 * @returns {void}
 */
const getClientConfig = (services) => (req, res) => {
  services.logger.info('[controller.getClientConfig] getting client config');

  const clientConfig = {};

  return res.send(`window.config = ${JSON.stringify(clientConfig)};`);
};

module.exports = {
  getClientConfig,
};
