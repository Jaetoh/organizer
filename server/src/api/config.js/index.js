'use strict';

const { Router: createRouter } = require('express');

const {
  getClientConfig,
} = require('./config.controller');

/**
 * Controllers registration function
 * @param {Object} services Services
 * @returns {Object} The configured Router
 */
module.exports = function register(services) {
  const router = createRouter();

  router.get('/', getClientConfig(services));

  return router;
};
