'use strict';

const { Joi } = require('../../utils/joi');

/**
 * Validate the request to update a user for an event
 *
 * @param {Request} req : the request
 * @returns {Object} query; the validated query
 */
function validatePatch(req) {
  const params = Joi.attempt(req.params, Joi.object({
    id: Joi.objectId().required(),
  }).required());

  const query = Joi.attempt(req.body, Joi.object({
    event_id: Joi.objectId().required(),
  }).required());

  return Object.assign({}, params, query);
}

module.exports = {
  validatePatch,
};
