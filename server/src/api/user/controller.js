'use strict';

const httpStatus = require('http-status-codes');

const schema = require('./schema');
const userModel = require('../../models/user');
const eventModel = require('../../models/event');

const ERRORS = Object.freeze({
  ALREADY_EXISTS: 'User is already added to this event as a Guest',
  BAD_ORGANIZATION: 'Event organization is not present in user organization',
  NOT_FOUND: 'User or Event not found',
});

/**
 * Add an event to a user
 *
 *
 * @param {Object} logger: logger service
 * @param {Object} mongodb: Mongodb connector
 * @returns {Function} The wrapped controller
 */
function addEvent({
  logger,
  mongodb,
}) {
/**
 * Add an event to a user
 *
 * @param {ExpressRequest} req Express request
 * @param {ExpressResponse} res Express response
 * @param {callback} next next
 *
 * @return {void}
 */
  return async (req, res) => {
    const payload = schema.validatePatch(req);

    logger.info({ payload }, '[user#addEvent] Adding event to a user');

    try {
      const [user, event] = await Promise.all([
        userModel.findOne(mongodb, payload.id),
        eventModel.findOne(mongodb, payload.event_id),
      ]);

      if (!event || !user) {
        logger.error({ event, user }, '[user#addEvent] Not found');
        return res.status(httpStatus.NOT_FOUND).send({ error: ERRORS.NOT_FOUND });
      }

      if (!user.organizations.map(obj => obj.toString())
        .includes(event.organization_id.toString())) {
        logger.error({ payload }, '[user#addEvent] Conflict with the organization id');
        return res.status(httpStatus.CONFLICT).send({ error: ERRORS.BAD_ORGANIZATION });
      }

      if (user.events.map(obj => obj.toString()).includes(payload.event_id.toString())) {
        logger.error({ payload }, '[user#addEvent] User is already in this event');
        return res.status(httpStatus.CONFLICT).send({ error: ERRORS.ALREADY_EXISTS });
      }

      await userModel
        .addEvent(mongodb, payload.event_id, payload.id);

      logger.info({ payload }, '[user#addEvent] Success');
      return res.status(httpStatus.NO_CONTENT).send();
    } catch (err) {
      logger.error({ err }, '[user#addEvent] Error while adding user to event');
      return res.status(httpStatus.INTERNAL_SERVER_ERROR)
        .send({ error: err.message });
    }
  };
}

module.exports = {
  addEvent,
};
