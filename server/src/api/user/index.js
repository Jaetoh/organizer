'use strict';

const { Router: createRouter } = require('express');

const wrapper = require('../../utils/wrapper');
const controller = require('./controller');

/**
 * Controllers registration function
 * @param {Object} services Services
 * @returns {Object} The configured Router
 */
module.exports = function register(services) {
  const router = createRouter();
  /**
   * @api {get} /api/user/:id/event Add an event to a user
   * @apiVersion 1.0.0
   * @apiGroup User
   *
   *
   * @apiParam {String} id The user id ( in path )
   * @apiParam {String} event_id The event id
   *
   *
   * @apiSuccessExample Success-Response:
   *  HTTP/1.1 204 NO CONTENT
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 409 CONFLICT
   * {
   *  "error": "User is already added to this event as a Guest"
   * }
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 404 NOT_FOUND
   * {
   *  "error": "User or Event not found"
   * }
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 500 INTERNAL SERVER ERROR
   * {
   *  "error": "SERVER_ERROR"
   * }
   */
  router.patch(
    '/:id/event',
    wrapper(controller.addEvent(services))
  );

  return router;
};

