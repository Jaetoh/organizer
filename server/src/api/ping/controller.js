'use strict';

/**
 * Ping/Pong
 *
 * @param {Request} req : the request
 * @param {Response} res : the response
 * @returns {void}
 */
function ping(req, res) {
  res.status(200).send('pong');
}

module.exports = {
  ping,
};
