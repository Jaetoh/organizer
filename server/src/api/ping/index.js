'use strict';

const { Router } = require('express');

const controller = require('./controller');

const router = Router();

/**
 * @api {get} /api/ping Ping route
 * @apiVersion 1.0.0
 * @apiGroup ping
 *
 * @apiDescription Check if organizer is available
 *
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 */
router.get('/', controller.ping);

module.exports = router;
