'use strict';

const express = require('express');

const clientConfig = require('./config.js');
const ping = require('./ping');
const events = require('./events');
const users = require('./users');
const user = require('./user');
const organization = require('./organization');


module.exports = function addRouter(app, services) {
  const router = express.Router();

  router.use('/config.js', clientConfig(services));
  router.use('/events', events(services));
  router.use('/user', user(services));
  router.use('/organization', organization(services));
  router.use('/users', users(services));
  router.use('/ping', ping);

  app.use('/api', router);
};
