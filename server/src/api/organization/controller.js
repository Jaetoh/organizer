'use strict';

const httpStatus = require('http-status-codes');

const schema = require('./schema');
const organizationModel = require('../../models/organization');

/**
 * Get an organization by its name
 *
 *
 * @param {Object} logger: logger service
 * @param {Object} mongodb: Mongodb connector
 * @returns {Function} The wrapped controller
 */
function get({
  logger,
  mongodb,
}) {
/**
 * Get an organization by its name
 *
 * @param {ExpressRequest} req Express request
 * @param {ExpressResponse} res Express response
 * @param {callback} next next
 *
 * @return {Object} The organization retrieved
 */
  return async (req, res) => {
    const payload = schema.validateGet(req);

    logger.info({ payload }, '[organizations#get] Getting Organizations');

    try {
      const organizations = await organizationModel
        .findOne(mongodb, payload.name);

      logger.info({ payload }, '[organizations#get] Success');
      return res.status(httpStatus.OK).send(organizations);
    } catch (err) {
      logger.error({ err }, '[organizations#get] Error while getting organizations');
      return res.status(httpStatus.INTERNAL_SERVER_ERROR)
        .send({ error: err.message });
    }
  };
}

module.exports = {
  get,
};
