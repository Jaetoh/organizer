'use strict';

const { Joi } = require('../../utils/joi');

/**
 * Validate the request to get an organization
 *
 * @param {Request} req : the request
 * @returns {Object} query; the validated query
 */
function validateGet(req) {
  const params = Joi.attempt(req.params, Joi.object({
    name: Joi.string().required(),
  }).required());

  return Object.assign({}, params);
}

module.exports = {
  validateGet,
};
