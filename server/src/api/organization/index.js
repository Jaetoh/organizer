'use strict';

const { Router: createRouter } = require('express');

const wrapper = require('../../utils/wrapper');
const controller = require('./controller');

/**
 * Controllers registration function
 * @param {Object} services Services
 * @returns {Object} The configured Router
 */
module.exports = function register(services) {
  const router = createRouter();
  /**
   * @api {get} /api/organizations Get one organization by its name
   * @apiVersion 1.0.0
   * @apiGroup Organization
   *
   *
   *
   * @apiSuccessExample Success-Response:
   *  HTTP/1.1 200
   * {
   *  _id: '5b4affd89a84d83056545403',
   *  name: "Toto",
   *  description: " I am a super company"
   *  location: "Paris",
   *  profile_image_url: 'http://test.com/test.png',
   *  cover_image_url: 'http://test.com/test.png',
   *  end_date: '2017-05-16T04:34:01.000Z',
   * }
   *
   *
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 500 INTERNAL SERVER ERROR
   * {
   *  "error": "SERVER_ERROR"
   * }
   */
  router.get(
    '/:name',
    wrapper(controller.get(services))
  );

  return router;
};

