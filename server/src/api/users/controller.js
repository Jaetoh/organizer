'use strict';

const httpStatus = require('http-status-codes');

const schema = require('./schema');
const userModel = require('../../models/user');

/**
 * Get a list of users by organization
 *
 *
 * @param {Object} logger: logger service
 * @param {Object} mongodb: Mongodb connector
 * @returns {Function} The wrapped controller
 */
function get({
  logger,
  mongodb,
}) {
/**
 * Get a list of users by organization
 *
 * @param {ExpressRequest} req Express request
 * @param {ExpressResponse} res Express response
 * @param {callback} next next
 *
 * @return {Array} User list
 */
  return async (req, res) => {
    const payload = schema.validateGet(req);

    logger.info({ payload }, '[users#get] Getting users by Organization');

    try {
      const users = await userModel
        .findByOrganization(mongodb, payload.organizationId);

      logger.info({ payload }, '[users#get] Success');
      return res.status(httpStatus.OK).send(users);
    } catch (err) {
      logger.error({ err }, '[users#get] Error while getting users');
      return res.status(httpStatus.INTERNAL_SERVER_ERROR)
        .send({ error: err.message });
    }
  };
}

module.exports = {
  get,
};
