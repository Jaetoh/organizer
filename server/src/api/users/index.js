'use strict';

const { Router: createRouter } = require('express');

const wrapper = require('../../utils/wrapper');
const controller = require('./controller');

/**
 * Controllers registration function
 * @param {Object} services Services
 * @returns {Object} The configured Router
 */
module.exports = function register(services) {
  const router = createRouter();
  /**
   * @api {get} /api/users/:organizationId Get a list of users by organization id
   * @apiVersion 1.0.0
   * @apiGroup Users
   *
   * @apiDescription Get a list of users with pagination parameters ( page, limit, sort )
   *
   * @apiParam {String} organizationId The organization id to look up
   *
   *
   * @apiSuccessExample Success-Response:
   *  HTTP/1.1 200
   * [{
   *  _id: '5b4affd89a84d83056545403',
   *  name: "Toto",
   *  email: "alexandre.abidri@test.com",
   *  image_url: "http://test.com/test.png",
   *  organizations: ['5b4affd89a84d83056545403'],
   *  events: ['5b4affd89a84d83056545403'],
   * }]
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 500 INTERNAL SERVER ERROR
   * {
   *  "error": "SERVER_ERROR"
   * }
   */
  router.get(
    '/:organizationId',
    wrapper(controller.get(services))
  );

  return router;
};

