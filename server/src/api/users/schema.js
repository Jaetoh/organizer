'use strict';

const { Joi } = require('../../utils/joi');

/**
 * Validate the request to get users by organization
 *
 * @param {Request} req : the request
 * @returns {Object} query; the validated query
 */
function validateGet(req) {
  const params = Joi.attempt(req.params, Joi.object({
    organizationId: Joi.objectId(),
  }).required());

  return Object.assign({}, params);
}

module.exports = {
  validateGet,
};
