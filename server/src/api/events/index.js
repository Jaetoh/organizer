'use strict';

const { Router: createRouter } = require('express');

const wrapper = require('../../utils/wrapper');
const controller = require('./controller');

/**
 * Controllers registration function
 * @param {Object} services Services
 * @returns {Object} The configured Router
 */
module.exports = function register(services) {
  const router = createRouter();
  /**
   * @api {get} /api/events Get a list of events by organization id
   * @apiVersion 1.0.0
   * @apiGroup Events
   *
   * @apiDescription Get a list of events with pagination parameters ( page and limit )
   * @apiParam {String} organizationId The organization id to look up
   *
   * @apiParam {Integer} page  The page index, **default is 1**
   * @apiParam {Integer} limit Number of items per page, **default is 10**
   * @apiParam {String} sort Field to sort by
   * @apiParam {Boolean} descending order of the sort ( True for descending )
   *
   * @apiParamExample Request-Example: Params
   * {
   *     "page" : 2,
   *     "limit" : 10,
   *     "sort":  "name"
   *     "descending": true
   * }
   *
   * @apiSuccessExample Success-Response:
   *  HTTP/1.1 200
   * [{
   *  _id: '5b4affd89a84d83056545403',
   *  name: 'Event 1',
   *  location: 'Paris',
   *  price: 300,
   *  image_url: 'http://test.com/test.png',
   *  organization_id: '5b4affd89a84d83056545403',
   *  start_date: '2017-05-15T04:34:01.000Z',
   *  end_date: '2017-05-16T04:34:01.000Z',
   * }]
   *
   * @apiErrorExample Error-Response:
   *  HTTP/1.1 500 INTERNAL SERVER ERROR
   * {
   *  "error": "SERVER_ERROR"
   * }
   */
  router.get(
    '/:organizationId',
    wrapper(controller.get(services))
  );

  return router;
};
