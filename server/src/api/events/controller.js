'use strict';

const httpStatus = require('http-status-codes');

const schema = require('./schema');
const eventModel = require('../../models/event');

/**
 * Get a list of events by organization
 *
 *
 * @param {Object} logger: logger service
 * @param {Object} mongodb: Mongodb connector
 * @returns {Function} The wrapped controller
 */
function get({
  logger,
  mongodb,
}) {
/**
 * Get a list of events by organization
 *
 * @param {ExpressRequest} req Express request
 * @param {ExpressResponse} res Express response
 * @param {callback} next next
 *
 * @return {Array} Event list
 */
  return async (req, res) => {
    const payload = schema.validateGet(req);

    logger.info({ payload }, '[events#get] Getting events by Organization');

    try {
      const events = await eventModel
        .findByOrganization(mongodb, payload);

      logger.info({ payload }, '[events#get] Success');
      return res.status(httpStatus.OK).send(events);
    } catch (err) {
      logger.error({ err }, '[events#get] Error while getting events');
      return res.status(httpStatus.INTERNAL_SERVER_ERROR)
        .send({ error: err.message });
    }
  };
}

module.exports = {
  get,
};
