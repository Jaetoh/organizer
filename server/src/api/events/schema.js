'use strict';

const { Joi } = require('../../utils/joi');

/**
 * Validate the request to get events by Organization
 *
 * @param {Request} req : the request
 * @returns {Object} query; the validated query
 */
function validateGet(req) {
  const params = Joi.attempt(req.params, Joi.object({
    organizationId: Joi.objectId().required(),
  }).required());

  const query = Joi.attempt(req.query, Joi.object().keys({
    page: Joi.number().integer().min(1).default(1),
    limit: Joi.number().integer().min(1).default(20),
    sort: Joi.string().default('_id'),
    descending: Joi.boolean().default(false),
  }).required());

  return Object.assign({}, params, query);
}

module.exports = {
  validateGet,
};
