'use strict';

const http = require('http');
const co = require('co');
const express = require('express');

const config = require('./config');
const expressConfig = require('./config/express');

const services = require('./services')(config);

let app;
let server;

/**
 * Start the web app.
 *
 * @returns {Promise} when app end to start
 */
async function start() {
  /* istanbul ignore next line */
  if (app) {
    return app;
  }

  app = expressConfig(express(), services);
  server = http.createServer(app);
  await server.listen(app.get('port'));

  await services.mongodb.connect();

  services.logger.info({
    port: server.address().port,
    environment: process.env.NODE_ENV,
  }, '✔ Server running');

  return app;
}

/**
 * Stop the web app.
 *
 * @returns {Promise} when app end to start
 */
async function stop() {
  /* istanbul ignore next line */
  if (server) {
    await server.close();
    server = null;
    app = null;
  }
  await services.mongodb.disconnect();
  return Promise.resolve();
}

/* istanbul ignore next line */
if (!module.parent) {
  co(start);
}

module.exports = {
  start,
  stop,
};
