'use strict';

const { ObjectId } = require('mongodb');

const MongoDbConnector = require('../src/services/mongo');
const logger = require('../src/services/logger');

const config = require('../src/config');

const eventModel = require('../src/models/event');
const organizationModel = require('../src/models/organization');
const userModel = require('../src/models/user');

/**
 * Init data for database
 * @returns {void}
 */
async function init() {
  const mongodb = new MongoDbConnector(config.services.mongodb, logger);

  await mongodb.connect();


  await eventModel.collection(mongodb).remove({});
  await organizationModel.collection(mongodb).remove({});
  await userModel.collection(mongodb).remove({});

  logger.info('Initial deployment: Collections removed');

  await Promise.all([
    userModel.createIndexes(mongodb),
    eventModel.createIndexes(mongodb),
    organizationModel.createIndexes(mongodb),
  ]);

  logger.info('Initial deployment: Indexes created');

  const events = [{
    _id: ObjectId('5b4affd89a84d83056545403'),
    name: 'HSBC GOLF',
    location: 'Dubai',
    price: 300,
    image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyrGqhol9iHW246T9AEUHNNls_LGUYO8xRQWBJGMm5UUcjoLUV',
    organization_id: ObjectId('5b4affd89a84d83056545403'),
    start_date: new Date('2017-02-01T06:34:01'),
    end_date: new Date('2017-02-10T06:34:01'),
  },
  {
    _id: ObjectId('5b4affd89a84d83056545401'),
    name: 'BWF World Tour',
    location: 'Paris',
    price: 400,
    image_url: 'http://cms.bwfbadminton.com/wp-content/uploads/2018/01/HSBC-BWF-World-Tour-Launch-Event-2.jpg',
    organization_id: ObjectId('5b4affd89a84d83056545403'),
    start_date: new Date('2017-05-15T06:34:01'),
    end_date: new Date('2017-05-16T06:34:01'),
  },
  {
    _id: ObjectId('5b4affd89a84d83056545400'),
    name: 'World Rugby Women’s Sevens Series 2019',
    location: 'Hong Kong',
    price: 600,
    image_url: 'https://pulse-static-files.s3.amazonaws.com/worldrugby/photo/2018/04/06/62b7c416-fee8-4340-91a1-1350addda581/ML20180406054.JPG',
    organization_id: ObjectId('5b4affd89a84d83056545403'),
    start_date: new Date('2018-08-01T06:34:01'),
    end_date: new Date('2018-09-16T06:34:01'),
  }, {
    _id: ObjectId('5b4affd89a84d83056545405'),
    name: 'U-17 World Cup',
    location: 'India',
    price: 1200,
    image_url: 'https://i.ndtvimg.com/i/2017-10/england-u17-win-afp_806x605_71509217178.jpg?output-quality=70&output-format=webp&downsize=555:*',
    organization_id: ObjectId('5b4a11d89a84d83056545403'),
    start_date: new Date('2017-10-15T06:34:01'),
    end_date: new Date('2017-10-30T06:34:01'),
  },
  {
    _id: ObjectId('5b4affd89a84d83056545408'),
    name: 'Women’s World Cup™ ',
    location: 'Canada',
    price: 690,
    image_url: 'https://img.fifa.com/mm/photo/tournament/competition/02/66/13/19/2661319_full-lnd.jpg',
    organization_id: ObjectId('5b4a11d89a84d83056545403'),
    start_date: new Date('2019-02-15T06:34:01'),
    end_date: new Date('2019-02-25T06:34:01'),
  },
  {
    _id: ObjectId('5b4affd89a84d83056545409'),
    name: 'Qatar 2022 World Cup',
    location: 'Qatar',
    price: 2000,
    image_url: 'https://www.thesun.co.uk/wp-content/uploads/2018/07/SPORT-PREVIEW-qatar-dates.jpg?strip=all&quality=100&w=1200&h=800&crop=1',
    organization_id: ObjectId('5b4a11d89a84d83056545403'),
    start_date: new Date('2022-10-15T06:34:01'),
    end_date: new Date('2022-11-16T06:34:01'),
  }];

  await Promise.all(events.map(event => eventModel.create(event, mongodb)));

  logger.info('Initial deployment: Initial events stored');

  const organizations = [{
    _id: ObjectId('5b4affd89a84d83056545403'),
    name: 'HSBC',
    description: 'HSBC is named after its founding member, The Hongkong and Shanghai Banking Corporation Limited, which was established in 1865 to finance the growing trade between Europe, India and China. The inspiration behind the founding of the bank was Thomas Sutherland, a Scot who was then working for the Peninsular and Oriental Steam Navigation Company. He realised that there was considerable demand for local banking facilities in Hong Kong and on the China coast, and he helped to establish the bank which opened in Hong Kong in March 1865 and in Shanghai a month later.',
    created_at: new Date('1950-08-14T06:34:01'),
    location: 'London',
    cover_image_url: 'https://i.dailymail.co.uk/i/pix/2012/07/30/article-0-025624BC000004B0-967_634x286.jpg',
    profile_image_url: 'https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/072011/hsbc-converted.png?itok=Rbsb5V2M',
  },
  {
    _id: ObjectId('5b4a11d89a84d83056545403'),
    name: 'FIFA',
    description: 'FIFA was founded in 1904 to oversee international competition among the national associations of Belgium, Denmark, France, Germany, the Netherlands, Spain, Sweden, and Switzerland. Headquartered in Zürich, its membership now comprises 211 national associations. Member countries must each also be members of one of the six regional confederations into which the world is divided: Africa, Asia, Europe, North & Central America and the Caribbean, Oceania, and South America.',
    created_at: new Date('1920-03-20T06:34:01'),
    location: 'Paris',
    cover_image_url: 'http://static.dnaindia.com/sites/default/files/styles/full/public/2018/07/15/704988-france-fifa-world-cup-2018-afp.jpg',
    profile_image_url: 'https://daily.jstor.org/wp-content/uploads/2015/06/FIFA_Logo_1050x700.jpg',
  }];

  await Promise.all(organizations
    .map(organization => organizationModel.create(organization, mongodb)));


  logger.info('Initial deployment: Initial organizations stored');

  const users = [{
    _id: ObjectId('5b4affd89a84d83056545403'),
    name: 'Jackie Chan',
    email: 'jackie@test.com',
    image_url: 'https://www.lemagsportauto.com/wp-content/uploads/2016/06/24H-du-Mans-Jackie-Chan-pr%C3%A9sent.jpg',
    events: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    organizations: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4a11d89a84d83056545403')],
  },
  {
    _id: ObjectId('5b4affd89a84d83056545401'),
    name: 'Steve Jobs',
    email: 'steve@test.com',
    image_url: 'https://i0.wp.com/www.seductionbykamal.com/wp-content/uploads/2012/02/STEVE_JOBS.jpg?resize=299%2C300&ssl=1',
    events: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    organizations: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4a11d89a84d83056545403')],
  },
  {
    _id: ObjectId('5b4affd89a84d83056545400'),
    name: 'Kylian Mbappé',
    email: 'kylian@test.com',
    image_url: 'https://amp.businessinsider.com/images/5b4c94fe45a56e2e008b4a16-750-562.jpg',
    events: [ObjectId('5b4affd89a84d83056545400'), ObjectId('5b4bffd89a84d83056545403'), ObjectId('5b4affd89a84d83056545401')],
    organizations: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4a11d89a84d83056545403')],
  },
  {
    _id: ObjectId('544affd89a84d83056545400'),
    name: 'Elon Musk',
    email: 'elon@test.com',
    image_url: 'https://i0.wp.com/iatranshumanisme.com/wp-content/uploads/2015/12/elon-musk.jpg?ssl=1',
    events: [ObjectId('5b4affd89a84d83056545400'), ObjectId('5b4bffd89a84d83056545403')],
    organizations: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4a11d89a84d83056545403')],
  }];

  await Promise.all(users.map(user => userModel.create(user, mongodb)));

  logger.info('Initial deployment: Initial users stored');

  await mongodb.disconnect();
}

init();
