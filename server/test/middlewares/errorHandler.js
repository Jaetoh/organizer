
'use strict';

const { expect } = require('chai');

const logger = require('../../src/services/logger');
const errorHandler = require('../../src/middlewares/errorHandler')({ logger });

describe('[middlewares] errorHandler', () => {
  it('should return no error (no error in the input)', () => {
    const result = errorHandler(null, null, null, () => false);

    expect(result).to.equal(false);
  });

  it('should return an error 400 ValidationError', (done) => {
    let body;
    let statusCode;
    const res = {
      status: code => {
        statusCode = code;
        return res;
      },
      send: json => {
        body = json;
        done();
      },
    };

    const error = new Error('ValidationError');
    error.name = 'ValidationError';
    error.details = [{ context: { key: 'some_field' }, message: 'Some validation error' }];

    errorHandler(error, null, res);

    expect(statusCode).to.equal(400);
    expect(body).to.have.nested.property('some_field.0', 'Some validation error');
  });

  it('should return an error 400 if status is send', (done) => {
    let body;
    let statusCode;
    const res = {
      status: code => {
        statusCode = code;
        return res;
      },
      send: json => {
        body = json;
        done();
      },
    };

    const error = new Error('Bad Request');
    error.name = 'Bad request';
    error.status = 400;
    error.details = [{ context: { key: 'some_field' }, message: 'Some error' }];

    errorHandler(error, null, res);

    expect(statusCode).to.equal(400);
    expect(body).to.have.nested.property('some_field.0', 'Some validation error');
  });

  it('should return an error 500', (done) => {
    let body;
    let statusCode;
    const res = {
      status: code => {
        statusCode = code;
        return res;
      },
      send: json => {
        body = json;
        done();
      },
    };

    const error = new Error('Some error message');
    errorHandler(error, null, res);

    expect(statusCode).to.equal(500);
    expect(body).to.have.property('detail', 'Some error message');
  });
});
