'use strict';

const { expect } = require('chai');

const wrapper = require('../../src/utils/wrapper');

describe('[Utils] Wrapper', () => {
  it('should return an async function', () => {
    const fn = wrapper((req, res) => {
      res.json({ foo: 'bar' });
    });
    expect(fn).to.exist();
  });

  it('should throw an error (no function passed to wrap)', () => {
    let error = null;
    try {
      wrapper();
    } catch (err) {
      error = err;
    }

    expect(error).to.not.be.null();
    expect(error).to.have.property('message', 'The function passed to wrap must not be null or undefined');
  });

  it('should call next without an error (controller)', async () => {
    const fn = wrapper((req, res, next) => {
      next();
    });

    let called = false;
    let error = null;
    await fn({}, { json: () => null }, (err) => {
      called = true;
      error = err;
    });

    expect(called).to.be.true();
    expect(error).to.not.be.null();
  });

  it('should call next without an error (middleware)', async () => {
    const fn = wrapper((err, req, res, next) => {
      next();
    });

    let called = false;
    let error = null;
    await fn(null, {}, { json: () => null }, (err) => {
      called = true;
      error = err;
    });

    expect(called).to.be.true();
    expect(error).to.not.be.null();
  });

  it('should throw an error (controller)', async () => {
    const fn = wrapper(() => {
      throw new Error('Foo bar');
    });

    let called = false;
    let error = null;
    await fn(null, {}, {}, (err) => {
      called = true;
      error = err;
    });

    expect(called).to.be.true();
    expect(error).to.not.be.null();
    expect(error).to.have.property('message').that.equals('Foo bar');
  });
});
