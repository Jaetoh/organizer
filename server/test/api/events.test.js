'use strict';

const { expect } = require('chai');
const request = require('supertest');
const sinon = require('sinon');
const { ObjectId } = require('mongodb');

const config = require('../../src/config');
const eventModel = require('../../src/models/event');

const MongoDbConnector = require('../../src/services/mongo');
const logger = require('../../src/services/logger');
const { start, stop } = require('../../src');

describe('[GET] /api/events/:organizationId', () => {
  const sandbox = sinon.sandbox.create();
  let app;
  const mongodb = new MongoDbConnector(config.services.mongodb, logger);

  before(async () => {
    await mongodb.connect();
    app = await start();
  });

  after(async () => {
    await mongodb.disconnect();
    await stop();
  });

  beforeEach(async () => {
    await eventModel.collection(mongodb).remove({});
    await eventModel.createIndexes(mongodb);
    const events = [{
      _id: ObjectId('5b4affd89a84d83056545403'),
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: ObjectId('5b4affd89a84d83056545403'),
      start_date: new Date('2017-05-15T06:34:01'),
      end_date: new Date('2017-05-16T06:34:01'),
    },
    {
      _id: ObjectId('5b4affd89a84d83056545401'),
      name: 'Event 2',
      location: 'Paris',
      price: 12389,
      image_url: 'http://test.com/test.png',
      organization_id: ObjectId('5b4affd89a84d83056545402'),
      start_date: new Date('2017-05-15T06:34:01'),
      end_date: new Date('2017-05-16T06:34:01'),
    },
    {
      _id: ObjectId('5b4affd89a84d83056545400'),
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: ObjectId('5b4affd89a84d83056545403'),
      start_date: new Date('2018-05-15T06:34:01'),
      end_date: new Date('2018-05-16T06:34:01'),
    }];
    await Promise.all(events.map(event => eventModel.create(event, mongodb)));
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should respond with a array of events', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545403')
      .query({ descending: true });

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545403',
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }, {
      _id: '5b4affd89a84d83056545400',
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2018-05-15T04:34:01.000Z',
      end_date: '2018-05-16T04:34:01.000Z',
    }]);
  });

  it('should respond with a array of events sorted by price', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545403')
      .query({ sort: 'price', descending: true });

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545400',
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2018-05-15T04:34:01.000Z',
      end_date: '2018-05-16T04:34:01.000Z',
    }, {
      _id: '5b4affd89a84d83056545403',
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }]);
  });

  it('should respond with a array of events sorted by date', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545403')
      .query({ sort: 'start_date', descending: true });

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545400',
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2018-05-15T04:34:01.000Z',
      end_date: '2018-05-16T04:34:01.000Z',
    }, {
      _id: '5b4affd89a84d83056545403',
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }]);
  });

  it('should respond with a array of events sorted by name', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545403')
      .query({ sort: 'name' });

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545403',
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }, {
      _id: '5b4affd89a84d83056545400',
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2018-05-15T04:34:01.000Z',
      end_date: '2018-05-16T04:34:01.000Z',
    }]);
  });

  it('should respond with a array of events sorted by location', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545403')
      .query({ sort: 'location', descending: true });

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545403',
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }, {
      _id: '5b4affd89a84d83056545400',
      name: 'Z-Event',
      location: 'Hong Kong',
      price: 456,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545403',
      start_date: '2018-05-15T04:34:01.000Z',
      end_date: '2018-05-16T04:34:01.000Z',
    }]);
  });

  it('should respond with an empty array', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545405');

    expect(status).to.equal(200);
    expect(body).to.deep.equal([]);
  });

  it('should respond with an event', async () => {
    const { body, status } = await request(app)
      .get('/api/events/5b4affd89a84d83056545402');

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545401',
      name: 'Event 2',
      location: 'Paris',
      price: 12389,
      image_url: 'http://test.com/test.png',
      organization_id: '5b4affd89a84d83056545402',
      start_date: '2017-05-15T04:34:01.000Z',
      end_date: '2017-05-16T04:34:01.000Z',
    }]);
  });

  it('should send back a 500 status code if something bad happened', async () => {
    sandbox.stub(eventModel, 'findByOrganization').throws(new Error('Unexpected error'));
    const { body, statusCode } = await request(app)
      .get('/api/events/5b4affd89a84d83056545402');

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 500,
      body: {
        error: 'Unexpected error',
      },
    });
  });
});
