'use strict';

const { expect } = require('chai');

const { start, stop } = require('../../src');

const request = require('supertest');

describe('[GET] /api/ping', () => {
  let app;
  before(async () => {
    app = await start();
  });

  after(async () => {
    await stop();
  });

  it('should respond pong', async () => {
    const { text, status } = await request(app)
      .get('/api/ping');

    expect(status).to.equal(200);
    expect(text).to.equal('pong');
  });
});
