'use strict';

const { expect } = require('chai');
const nock = require('nock');
const sinon = require('sinon');

const { start, stop } = require('../../src');

const request = require('supertest');

describe('[GET] /api/config.js', () => {
  let app;
  const sandbox = sinon.sandbox.create();

  before(async () => {
    app = await start();
  });

  after(async () => {
    await stop();
  });

  afterEach(() => {
    nock.cleanAll();
    sandbox.restore();
  });

  describe('success', () => {
    it('Returns the client side config', async () => {
      const { text, status } = await request(app)
        .get('/api/config.js');

      expect(status).to.equal(200);
      expect(text).deep.equal('window.config = {};');
    });
  });
});
