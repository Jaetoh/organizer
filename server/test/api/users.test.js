'use strict';

const { expect } = require('chai');
const request = require('supertest');
const sinon = require('sinon');
const { ObjectId } = require('mongodb');

const config = require('../../src/config');
const userModel = require('../../src/models/user');

const MongoDbConnector = require('../../src/services/mongo');
const logger = require('../../src/services/logger');
const { start, stop } = require('../../src');

describe('[GET] /api/users/:organizationId', () => {
  const sandbox = sinon.sandbox.create();
  let app;
  const mongodb = new MongoDbConnector(config.services.mongodb, logger);

  before(async () => {
    await mongodb.connect();
    app = await start();
  });

  after(async () => {
    await mongodb.disconnect();
    await stop();
  });

  beforeEach(async () => {
    await userModel.createIndexes(mongodb);
    await userModel.collection(mongodb).remove({});
    const users = [{
      _id: ObjectId('5b4affd89a84d83056545403'),
      name: 'User 1',
      email: 'user1@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    },
    {
      _id: ObjectId('5b4affd89a84d83056545401'),
      name: 'User 2',
      email: 'user2@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    },
    {
      _id: ObjectId('5b4affd89a84d83056545400'),
      name: 'User 3',
      email: 'user3@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b4affd89a84d83056545400'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    },
    {
      _id: ObjectId('544affd89a84d83056545400'),
      name: 'User 4',
      email: 'user4@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b4affd89a84d83056545400'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
    }];
    await Promise.all(users.map(user => userModel.create(user, mongodb)));
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should respond with a array of users', async () => {
    const { body, status } = await request(app)
      .get('/api/users/5b5affd89a84d83056545403');

    expect(status).to.equal(200);
    expect(body).to.deep.equal([{
      _id: '5b4affd89a84d83056545403',
      name: 'User 1',
      email: 'user1@test.com',
      image_url: 'http://test.com/test.png',
      events: ['5b5affd89a84d83056545403', '5b4bffd89a84d83056545403'],
      organizations: ['5b5affd89a84d83056545403', '5b4bffd89a84d83056545403'],
    },
    {
      _id: '5b4affd89a84d83056545401',
      name: 'User 2',
      email: 'user2@test.com',
      image_url: 'http://test.com/test.png',
      events: ['5b4affd89a84d83056545403', '5b4bffd89a84d83056545403'],
      organizations: ['5b5affd89a84d83056545403', '5b4bffd89a84d83056545403'],
    },
    {
      _id: '5b4affd89a84d83056545400',
      name: 'User 3',
      email: 'user3@test.com',
      image_url: 'http://test.com/test.png',
      events: ['5b4affd89a84d83056545400', '5b4bffd89a84d83056545403'],
      organizations: ['5b5affd89a84d83056545403', '5b4bffd89a84d83056545403'],
    },
    {
      _id: '544affd89a84d83056545400',
      name: 'User 4',
      email: 'user4@test.com',
      image_url: 'http://test.com/test.png',
      events: ['5b4affd89a84d83056545400', '5b4bffd89a84d83056545403'],
      organizations: ['5b5affd89a84d83056545403', '5b4bffd89a84d83056545403'],
    }]);
  });

  it('should respond with an empty array', async () => {
    const { body, status } = await request(app)
      .get('/api/users/5b4affd89a84d83056545405');

    expect(status).to.equal(200);
    expect(body).to.deep.equal([]);
  });

  it('should send back a 500 status code if something bad happened', async () => {
    sandbox.stub(userModel, 'findByOrganization').throws(new Error('Unexpected error'));
    const { body, statusCode } = await request(app)
      .get('/api/users/5b4affd89a84d83056545402');

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 500,
      body: {
        error: 'Unexpected error',
      },
    });
  });
});
