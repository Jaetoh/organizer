'use strict';

const { expect } = require('chai');
const request = require('supertest');
const sinon = require('sinon');
const { ObjectId } = require('mongodb');

const config = require('../../src/config');
const organizationModel = require('../../src/models/organization');

const MongoDbConnector = require('../../src/services/mongo');
const logger = require('../../src/services/logger');
const { start, stop } = require('../../src');

describe('[GET] /api/organization/:name', () => {
  const sandbox = sinon.sandbox.create();
  let app;
  const mongodb = new MongoDbConnector(config.services.mongodb, logger);

  before(async () => {
    await mongodb.connect();
    app = await start();
  });

  after(async () => {
    await mongodb.disconnect();
    await stop();
  });

  beforeEach(async () => {
    await organizationModel.collection(mongodb).remove({});
    await organizationModel.createIndexes(mongodb);
    const organizations = [{
      _id: ObjectId('5b4affd89a84d83056545403'),
      name: 'HSBC',
      description: 'I am a description',
      created_at: new Date('2018-05-15T06:34:01'),
      location: 'London',
      cover_image_url: 'http://test.com/test.png',
      profile_image_url: 'http://test.com/test.png',
    }];
    await Promise.all(organizations
      .map(organization => organizationModel.create(organization, mongodb)));
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should respond with a array of users', async () => {
    const { body, status } = await request(app)
      .get('/api/organization/HSBC');

    expect(status).to.equal(200);
    expect(body).to.deep.equal({
      _id: '5b4affd89a84d83056545403',
      name: 'HSBC',
      description: 'I am a description',
      created_at: '2018-05-15T04:34:01.000Z',
      location: 'London',
      cover_image_url: 'http://test.com/test.png',
      profile_image_url: 'http://test.com/test.png',
    });
  });

  it('should respond with empty body', async () => {
    const { body, status } = await request(app)
      .get('/api/organization/HSBCS');

    expect(status).to.equal(200);
    expect(body).to.deep.equal({});
  });

  it('should send back a 500 status code if something bad happened', async () => {
    sandbox.stub(organizationModel, 'findOne').throws(new Error('Unexpected error'));
    const { body, statusCode } = await request(app)
      .get('/api/organization/HSBC');

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 500,
      body: {
        error: 'Unexpected error',
      },
    });
  });
});
