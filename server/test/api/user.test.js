'use strict';

const { expect } = require('chai');
const request = require('supertest');
const sinon = require('sinon');
const { ObjectId } = require('mongodb');

const config = require('../../src/config');
const userModel = require('../../src/models/user');
const eventModel = require('../../src/models/event');

const MongoDbConnector = require('../../src/services/mongo');
const logger = require('../../src/services/logger');
const { start, stop } = require('../../src');

describe('[PATCH] /api/users/:id', () => {
  const sandbox = sinon.sandbox.create();
  let app;
  const mongodb = new MongoDbConnector(config.services.mongodb, logger);

  before(async () => {
    await mongodb.connect();
    app = await start();
  });

  after(async () => {
    await mongodb.disconnect();
    await stop();
  });

  beforeEach(async () => {
    await Promise.all([
      userModel.createIndexes(mongodb),
      eventModel.createIndexes(mongodb),
    ]);

    await userModel.collection(mongodb).remove({});
    await eventModel.collection(mongodb).remove({});
    const events = [{
      _id: ObjectId('5b5affd89a84d83056545403'),
      name: 'Event 1',
      location: 'Paris',
      price: 300,
      image_url: 'http://test.com/test.png',
      organization_id: ObjectId('5b4affd89a84d83056545403'),
      start_date: new Date('2017-05-15T06:34:01'),
      end_date: new Date('2017-05-16T06:34:01'),
    },
    {
      _id: ObjectId('5b4affd89a84d83056545401'),
      name: 'Event 2',
      location: 'Paris',
      price: 12389,
      image_url: 'http://test.com/test.png',
      organization_id: ObjectId('5b4affd89a84d83056545402'),
      start_date: new Date('2017-05-15T06:34:01'),
      end_date: new Date('2017-05-16T06:34:01'),
    }];
    await Promise.all(events.map(event => eventModel.create(event, mongodb)));
    const users = [{
      _id: ObjectId('5b4affd89a84d83056545403'),
      name: 'User 1',
      email: 'user1@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4affd89a84d83056545402')],
    },
    {
      _id: ObjectId('5b4affd89a84d83056545401'),
      name: 'User 2',
      email: 'user2@test.com',
      image_url: 'http://test.com/test.png',
      events: [ObjectId('5b5affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403')],
      organizations: [ObjectId('5b4affd89a84d83056545403')],
    }];
    await Promise.all(users.map(user => userModel.create(user, mongodb)));
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should respond with 204 status code', async () => {
    const { status } = await request(app)
      .patch('/api/user/5b4affd89a84d83056545403/event')
      .send({ event_id: ObjectId('5b4affd89a84d83056545401') });

    const user = await userModel.findOne(mongodb, ObjectId('5b4affd89a84d83056545403'));

    expect(status).to.equal(204);
    expect(user.events).to.deep
      .equal([ObjectId('5b4affd89a84d83056545403'), ObjectId('5b4bffd89a84d83056545403'), ObjectId('5b4affd89a84d83056545401')]);
  });

  it('should respond with 404 because event has not been found', async () => {
    const { statusCode, body } = await request(app)
      .patch('/api/user/5b4affd89a84d83056545403/event')
      .send({ event_id: ObjectId('5b6affd89a84d83056545403') });

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 404,
      body: {
        error: 'User or Event not found',
      },
    });
  });

  it('should respond with 404 because user has not been found', async () => {
    const { statusCode, body } = await request(app)
      .patch('/api/user/5b4affd89a83d83056545403/event')
      .send({ event_id: ObjectId('5b5affd89a84d83056545403') });

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 404,
      body: {
        error: 'User or Event not found',
      },
    });
  });

  it('should respond with 409 because event doesnt belong to the user organization', async () => {
    const { statusCode, body } = await request(app)
      .patch('/api/user/5b4affd89a84d83056545403/event')
      .send({ event_id: ObjectId('5b5affd89a84d83056545403') });

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 409,
      body: {
        error: 'Event organization is not present in user organization',
      },
    });
  });

  it('should respond with 409 because event doesnt belong to the user organization', async () => {
    const { statusCode, body } = await request(app)
      .patch('/api/user/5b4affd89a84d83056545401/event')
      .send({ event_id: ObjectId('5b5affd89a84d83056545403') });

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 409,
      body: {
        error: 'User is already added to this event as a Guest',
      },
    });
  });

  it('should send back a 500 status code if something bad happened', async () => {
    sandbox.stub(userModel, 'findOne').throws(new Error('Unexpected error'));
    const { statusCode, body } = await request(app)
      .patch('/api/user/5b4affd89a84d83056545401/event')
      .send({ event_id: ObjectId('5b5affd89a84d83056545403') });

    expect({ statusCode, body }).to.deep.equal({
      statusCode: 500,
      body: {
        error: 'Unexpected error',
      },
    });
  });
});
