# Organizer

## Documentation :orange_book:

`http://localhost:3001/apidoc`

## Deployment :ship:

1. [Install Docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-14-04)
2. Deploy the app
```bash
./start.sh
```
3. Go on `http://localhost:3001/`

Each time the server is deployed, data are removed and initialized at the initial State.

## Installation

prerequisites: [Lerna](https://lernajs.io/) and [Yarn](https://yarnpkg.com/en/) (as global)

```bash
nvm i
npm install --global lerna
lerna bootstrap
```

```bash
brew install yarn
```

```bash
sudo apt-get update && sudo apt-get install yarn
```


## Run in dev mode

```bash
lerna run start:dev --stream
```

This will start the front-end with it's own server which will proxy `/api/*` requests on the backend.

## Run in production

```bash
lerna run start
```

In production mode the backend serves the static assets itself

## Start Storybook

```bash
lerna run storybook --stream
```

## Tests :boom:

To be able to run the tests, you need to setup mongodb in a container by running `docker-compose-unit-tests.yml` at the root of the project
For both client/server sides:

```bash
CI=true lerna run test
```

For client side ( client folder ):

```bash
yarn run test --coverage
```

For server side ( server folder ):

```bash
yarn run test
```
